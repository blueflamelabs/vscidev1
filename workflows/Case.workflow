<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Comment_Notification_to_Case_Creator</fullName>
        <description>Case Comment Notification to Case Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>vforceadmin@virtualinc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Comment_Creation_Notification_To_CreatedBy</template>
    </alerts>
</Workflow>
