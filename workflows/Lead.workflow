<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Membership_Interest</fullName>
        <description>New Membership Interest</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Emails/Membership_Interest_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_notification_to_Qualify_User</fullName>
        <ccEmails>keinhaus@virtualinc.com</ccEmails>
        <description>Send notification to Qualify User</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Emails/LeadsFormassemblyQualifyResponse</template>
    </alerts>
</Workflow>
