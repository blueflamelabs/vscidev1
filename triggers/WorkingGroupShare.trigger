trigger WorkingGroupShare on Working_Group__c (after insert) {
    AssociationRecordShareCTRL.WorkingGroupRecord(Trigger.new);
}