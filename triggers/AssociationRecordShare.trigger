trigger AssociationRecordShare on Association_Team__c (after insert,after update,after delete) {
    if(Trigger.isInsert && Trigger.isAfter){
        AssociationRecordShareCTRL.recordShare(Trigger.new);
    }
    if(Trigger.isUpdate && Trigger.isAfter){
        List<Association_Team__c> objAssTeamList = new List<Association_Team__c>();
        for(Association_Team__c objAssTeam : Trigger.new){
            if(String.isNotBlank(objAssTeam.User__c) && objAssTeam.User__c != Trigger.oldMap.get(objAssTeam.id).User__c){
                objAssTeamList.add(objAssTeam);
            }
        }
        if(objAssTeamList.size() > 0){
            AssociationRecordShareCTRL.recordShare(objAssTeamList);
        }
    }
    if(Trigger.isDelete && Trigger.isAfter){
        AssociationRecordShareCTRL.RecordDelete(Trigger.Old);
    }
}