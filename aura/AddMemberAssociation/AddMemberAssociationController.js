({
     doInit: function(component, event, helper) {        
       helper.getAssMemberTypehelper(component, event);
       helper.getBaseUrl(component, event);
       helper.getUITheme(component, event);
       helper.getAssCorporateMember(component, event,'Active'); 
    },
    selectedCorporateMemberPicklist: function(component, event, helper) {
         var selectedVal=component.get('v.SelectedPicklistVal');
        helper.getAssCorporateMember(component, event,selectedVal);
    },
    SelectMember: function(component, event, helper) {        
       // helper.getMemberTypePicklist(component, event);
         helper.getAssMemberTypehelper(component, event);
    },
	 NoAction : function(component, event, helper) {
        var selectVal=component.get('v.value');
         if(selectVal=="option1"){
         	component.set('v.SelectedPage','1');
            component.set('v.HeaderName','Add Corporate Member');
         }
        if(selectVal=="option2"){
         	component.set('v.SelectedPage','2');
            component.set('v.pcMemberCategory',false);
            component.set('v.HeaderName','Add Individual Member');
         } 
        if(selectVal=="option3"){
         	component.set('v.SelectedPage','2');
            component.set('v.pcMemberCategory',false);
            component.set('v.HeaderName','Add Staff Member');
        } 
        if(selectVal=="option4"){
         	component.set('v.SelectedPage','7');
            component.set('v.HeaderName','Add Corporate Representative');
        } 
    },
    SelectCorporateMemeberNext :function(component, event, helper) {
        var corporateMemeberId =component.get("v.AssCorporateMemberId");
            // alert(corporateMemeberId);
         var isvalid=true;
         if(corporateMemeberId==null){
             isvalid=false;
             component.set("v.messageForselectCorporateMember", "You need to select Corporate Memeber.");
            }	    
            if(isvalid == true){ 
             component.set('v.SelectedPage','2');
             component.set('v.pcMemberCategory',true);
             component.set('v.requiredField',false);  
            }
    },
    NextForExiting : function(component, event, helper) { 
        var domain = component.find("domainName");
        var domainVal = domain.get("v.value");
        var corporateAccVal = component.get("v.existingCorporateAccount");
      //  alert(domainVal);
        //alert(corporateAccVal);
      
        var isvalid = true;
        if(domainVal == null && corporateAccVal == null){
            isvalid=false;
            domain.setCustomValidity("You need to Enter Corporate Account Domain");
            domain.reportValidity();
        }
        if(domainVal != null && corporateAccVal == null){
           isvalid=false;
                domain.setCustomValidity(" ");
                domain.reportValidity();
                $A.util.removeClass(domain, "slds-has-error"); 
                $A.util.addClass(domain, "hide-error-message");
           component.set("v.message", "You need to select Corporate Account.");
        }
        
        if(isvalid == true){
         component.set('v.SelectedPage','2');
         component.set('v.pcMemberCategory',false);
        }
    },
    PrimaryContactNext : function(component, event, helper) { 
        var selectVal=component.get('v.value');
        var pcMemberCategory=component.get('v.pcMemberCategory');
                var firstNameId = component.find("firstName");
                var firstNameVal = firstNameId.get("v.value");
                var lastNameId = component.find("lastName");
                var lastNameVal = lastNameId.get("v.value");
                var emailId = component.find("emailId");
                var emailVal = emailId.get("v.value");
                var pcBillingStreetId = component.find("pcBillingStreet");
                var pcBillingStreetVal = pcBillingStreetId.get("v.value");
                var pcBillingCityId = component.find("pcBillingCity");
                var pcBillingCityVal = pcBillingCityId.get("v.value");
                var pcbillingStateId = component.find("pcbillingState");
                var pcbillingStateVal = pcbillingStateId.get("v.value");
        		var pcbillingCountryId = component.find("pcbillingCountry");
                var pcbillingCountryVal = pcbillingCountryId.get("v.value");
          		var pcbillingPostalCodeId = component.find("pcbillingPostalCode");
                var pcbillingPostalCodeVal = pcbillingPostalCodeId.get("v.value");
        
                var isvalid = true;
                if(firstNameVal == ''){
                    isvalid=false;
                    firstNameId.setCustomValidity("You need to Enter First Name");
                    firstNameId.reportValidity();
                }else{
                    firstNameId.setCustomValidity("");
                    firstNameId.reportValidity();
                }
                if(lastNameVal == ''){
                    isvalid=false;
                    lastNameId.setCustomValidity("You need to Enter Last Name");
                    lastNameId.reportValidity();
                }else{
                    lastNameId.setCustomValidity("");
                    lastNameId.reportValidity();
                }
                if(emailVal == ''){
                    isvalid=false;
                    emailId.setCustomValidity("You need to Enter Email");
                    emailId.reportValidity();
                }else{
                    emailId.setCustomValidity("");
                    emailId.reportValidity();
                }
        		if(pcBillingStreetVal == ''){
                    isvalid=false;
                    pcBillingStreetId.setCustomValidity("You need to Enter Billing Street");
                    pcBillingStreetId.reportValidity();
                }else{
                    pcBillingStreetId.setCustomValidity("");
                    pcBillingStreetId.reportValidity();
                }
        		if(pcBillingCityVal == ''){
                    isvalid=false;
                    pcBillingCityId.setCustomValidity("You need to Enter Billing City");
                    pcBillingCityId.reportValidity();
                }else{
                    pcBillingCityId.setCustomValidity("");
                    pcBillingCityId.reportValidity();
                }
                if(pcbillingStateVal == ''){
                    isvalid=false;
                    pcbillingStateId.setCustomValidity("You need to Enter Billing State");
                    pcbillingStateId.reportValidity();
                }else{
                    pcbillingStateId.setCustomValidity("");
                    pcbillingStateId.reportValidity();
                }
                if(pcbillingCountryVal == ''){
                    isvalid=false;
                    pcbillingCountryId.setCustomValidity("You need to Enter Billing Country");
                    pcbillingCountryId.reportValidity();
                }else{
                    pcbillingCountryId.setCustomValidity("");
                    pcbillingCountryId.reportValidity();
                }
                if(pcbillingPostalCodeVal == ''){
                    isvalid=false;
                    pcbillingPostalCodeId.setCustomValidity("You need to Enter Billing Postal Code");
                    pcbillingPostalCodeId.reportValidity();
                }else{
                    pcbillingPostalCodeId.setCustomValidity("");
                    pcbillingPostalCodeId.reportValidity();
                }
        if(isvalid == true){        
           if(selectVal=="option1"){ 
                 component.set('v.SelectedPage','3');
                 component.set('v.primaryContactFirstName',firstNameVal);
                 component.set('v.primaryContactLastName',lastNameVal);
               
            }else{
                  component.set('v.SelectedPage','6');
            }
       } 
    },
    AlternateContactNext : function(component, event, helper) { 
        var accuntval=component.get('v.existingCorporateAccount') 
            if(accuntval!=null){
             //component.set('v.existiCorporatAccount',true);
            } 
        var val = component.find("alternateContact").get("v.value");
       
        if(!val){
        var firstNameAltId = component.find("firstNameAlt");
        var firstNameAltVal = firstNameAltId.get("v.value");
        var lastNameAltId = component.find("lastNameAlt");
        var lastNameAltVal = lastNameAltId.get("v.value");
        var emailAltId = component.find("emailIdAlt");
        var emailAltVal = emailAltId.get("v.value");
        var isvalid = true;
        if(firstNameAltVal == ''){
            isvalid=false;
            firstNameAltId.setCustomValidity("You need to Enter First Name");
            firstNameAltId.reportValidity();
        }else{
            firstNameAltId.setCustomValidity("");
            firstNameAltId.reportValidity();
        }
        if(lastNameAltVal == ''){
            isvalid=false;
            lastNameAltId.setCustomValidity("You need to Enter Last Name");
            lastNameAltId.reportValidity();
        }else{
            lastNameAltId.setCustomValidity("");
            lastNameAltId.reportValidity();
        }
        if(emailAltVal == ''){
            isvalid=false;
            emailAltId.setCustomValidity("You need to Enter Email");
            emailAltId.reportValidity();
        }else{
            emailAltId.setCustomValidity("");
            emailAltId.reportValidity();
        } 
        if(isvalid == true){
             component.set('v.SelectedPage','4');
             component.set('v.alteContactCheckBox',true);
             component.set('v.altContactFirstName',firstNameAltVal);
             component.set('v.altContactLastName',lastNameAltVal);
            
          }
        }else{
            component.set('v.SelectedPage','4'); 
            component.set('v.alteContactCheckBox',false);
        }   
    },
    BillingContactNext : function(component, event, helper) { 
        var selectVal = component.get("v.billingContact");
        var corprateAcc = component.get("v.existingCorporateAccount");
        var newBillcont = component.get("v.newBillContact");
       // alert(corprateAcc);
        if(selectVal == "Other" && newBillcont){
                var firstNameBillId = component.find("firstNameBill");
                var firstNameBillVal = firstNameBillId.get("v.value");
                var lastNameBillId = component.find("lastNameBill");
                var lastNameBillVal = lastNameBillId.get("v.value");
                var emailBillId = component.find("emailIdBill");
                var emailBillVal = emailBillId.get("v.value");
                var isvalid = true;
                if(firstNameBillVal == ''){
                    isvalid=false;
                    firstNameBillId.setCustomValidity("You need to Enter First Name");
                    firstNameBillId.reportValidity();
                }else{
                    firstNameBillId.setCustomValidity("");
                    firstNameBillId.reportValidity();
                }
                if(lastNameBillVal == ''){
                    isvalid=false;
                    lastNameBillId.setCustomValidity("You need to Enter Last Name");
                    lastNameBillId.reportValidity();
                }else{
                    lastNameBillId.setCustomValidity("");
                    lastNameBillId.reportValidity();
                }
                if(emailBillVal == ''){
                    isvalid=false;
                    emailBillId.setCustomValidity("You need to Enter Email");
                    emailBillId.reportValidity();
                }else{
                    emailBillId.setCustomValidity("");
                    emailBillId.reportValidity();
                } 
                if(isvalid == true){
                 component.set('v.SelectedPage','5');
                }
        } else{
            component.set('v.SelectedPage','5');
        }     
    },
    CreateBillContact :function(component, event, helper) {
          var selectVal = component.get("v.billingContact");
        //alert(selectVal);
            if(selectVal == "Other"){
                component.set('v.newBillContact',true);
                component.set('v.existiCorporatAccount',false);
                component.set('v.corporatePersonAccountList', null);
            }
    },
    LegalContactNext : function(component, event, helper) { 
        var selectVal = component.get("v.legalContact");
        var corprateAcc = component.get("v.existingCorporateAccount");
        var newlegalCon=  component.get("v.newLegalContact");
        // alert(corprateAcc);
            if(selectVal == "Other" && newlegalCon){
                var firstNameLegalId = component.find("firstNameLegal");
                var firstNameLegalVal = firstNameLegalId.get("v.value");
                var lastNameLegalId = component.find("lastNameLegal");
                var lastNameLegalVal = lastNameLegalId.get("v.value");
                var emailIdLegalId = component.find("emailIdLegal");
                var emailIdLegalVal = emailIdLegalId.get("v.value");
                var isvalid = true;
                    if(firstNameLegalVal == ''){
                        isvalid=false;
                        firstNameLegalId.setCustomValidity("You need to Enter First Name");
                        firstNameLegalId.reportValidity();
                    }else{
                        firstNameLegalId.setCustomValidity("");
                        firstNameLegalId.reportValidity();
                    }
                    if(lastNameLegalVal == ''){
                        isvalid=false;
                        lastNameLegalId.setCustomValidity("You need to Enter Last Name");
                        lastNameLegalId.reportValidity();
                    }else{
                        lastNameLegalId.setCustomValidity("");
                        lastNameLegalId.reportValidity();
                    }
                    if(emailIdLegalVal == ''){
                        isvalid=false;
                        emailIdLegalId.setCustomValidity("You need to Enter Email");
                        emailIdLegalId.reportValidity();
                    }else{
                        emailIdLegalId.setCustomValidity("");
                        emailIdLegalId.reportValidity();
                    } 
                    if(isvalid == true){
                       component.set('v.SelectedPage','6');
                    }
               }else{
                    component.set('v.SelectedPage','6');
               }  
    },
    CreateLegalContact:function(component, event, helper) {
		  var selectVal = component.get("v.legalContact");
		 if(selectVal == "Other"){
                component.set('v.newLegalContact',true);
                component.set('v.existiCorporatAccount',false);
                component.set('v.corporatePersonAccountList', null);
            }        
    },
    CorporateAccNext : function(component, event, helper) {
        
        var companyId = component.find("companyName");
        var companyVal = companyId.get("v.value");
        var corporateBillingStreetId=component.find("CorporateBillingStreet");
        var corporateBillingStreetVal= corporateBillingStreetId.get("v.value"); 
        var corporateBillingCityId=component.find("CorporateBillingCity");
        var corporateBillingCityVal= corporateBillingCityId.get("v.value"); 
        var corporateBillingStateId=component.find("CorporateBillingState");
        var corporateBillingStateVal= corporateBillingStateId.get("v.value"); 
        var corporateBillingPostalCodeId=component.find("CorporateBillingPostalCode");
        var corporateBillingPostalCodeVal= corporateBillingPostalCodeId.get("v.value"); 
        var corporateBillingCountryId=component.find("CorporateBillingCountry");
        var corporateBillingCountryVal= corporateBillingCountryId.get("v.value"); 
        var isvalid = true;
        if(companyVal == ''){
            isvalid=false;
            companyId.setCustomValidity("You need to Enter Company Name");
            companyId.reportValidity();
        }else{
            companyId.setCustomValidity("");
            companyId.reportValidity();
        } 
        if(corporateBillingStreetVal == ''){
            isvalid=false;
            corporateBillingStreetId.setCustomValidity("You need to Enter Billing Street");
            corporateBillingStreetId.reportValidity();
        }else{
            corporateBillingStreetId.setCustomValidity("");
            corporateBillingStreetId.reportValidity();
        } 
        if(corporateBillingCityVal == ''){
            isvalid=false;
            corporateBillingCityId.setCustomValidity("You need to Enter Billing City");
            corporateBillingCityId.reportValidity();
        }else{
            corporateBillingCityId.setCustomValidity("");
            corporateBillingCityId.reportValidity();
        } 
        if(corporateBillingStateVal == ''){
            isvalid=false;
            corporateBillingStateId.setCustomValidity("You need to Enter Billing State");
            corporateBillingStateId.reportValidity();
        }else{
            corporateBillingStateId.setCustomValidity("");
            corporateBillingStateId.reportValidity();
        } 
        if(corporateBillingPostalCodeVal == ''){
            isvalid=false;
            corporateBillingPostalCodeId.setCustomValidity("You need to Enter Billing Postal Code");
            corporateBillingPostalCodeId.reportValidity();
        }else{
            corporateBillingPostalCodeId.setCustomValidity("");
            corporateBillingPostalCodeId.reportValidity();
        } 
        if(corporateBillingCountryVal == ''){
            isvalid=false;
            corporateBillingCountryId.setCustomValidity("You need to Enter Billing Country");
            corporateBillingCountryId.reportValidity();
        }else{
            corporateBillingCountryId.setCustomValidity("");
            corporateBillingCountryId.reportValidity();
        } 
         if(isvalid == true){
           component.set('v.SelectedPage','2');
            component.set('v.pcMemberCategory',false);  
            var CorporatebillingStreetId = component.find("CorporateBillingStreet");
            var CorporatebillingStreetVal = CorporatebillingStreetId.get("v.value");
            var billingStreetId = component.find("pcBillingStreet");
            billingStreetId.set("v.value",CorporatebillingStreetVal);
             
            var CorporateBillingCityId = component.find("CorporateBillingCity");
            var CorporateBillingCityVal = CorporateBillingCityId.get("v.value");
            var BillingCityId = component.find("pcBillingCity");
            BillingCityId.set("v.value",CorporateBillingCityVal);
             
            var CorporateBillingStateId = component.find("CorporateBillingState");
            var CorporateBillingStateVal = CorporateBillingStateId.get("v.value");
            var billingStateId = component.find("pcbillingState");
            billingStateId.set("v.value",CorporateBillingStateVal);
             
            var CorporateBillingCountryId = component.find("CorporateBillingCountry");
            var CorporateBillingCountryVal = CorporateBillingCountryId.get("v.value");
            var billingCountryId = component.find("pcbillingCountry");
            billingCountryId.set("v.value",CorporateBillingCountryVal);
             
            var CorporateBillingPostalCodeId = component.find("CorporateBillingPostalCode");
            var CorporateBillingPostalCodeVal = CorporateBillingPostalCodeId.get("v.value");
            var billingPostalCodeId = component.find("pcbillingPostalCode");
            billingPostalCodeId.set("v.value",CorporateBillingPostalCodeVal);
        }
    },
    CreateNewNext : function(component, event, helper) {
        component.set("v.existingCorporateAccount",null);
        var selectVal=component.get('v.nextForNew');
         component.set('v.nextForNew',true);
    },
    FindDomain : function(component, event, helper) {
       component.set("v.spinner", false);
        
        var domainVal=component.get('v.domainName');
        var domain = component.find("domainName");
        var domainVal = domain.get("v.value");
        if(domainVal!=null){
           domain.setCustomValidity(" ");
           domain.reportValidity(); 
           $A.util.removeClass(domain, "slds-has-error"); 
        }
         helper.FindDomainHelper(component, event, domainVal);
    },
     FindCorporateMember : function(component, event, helper) {
          component.set("v.spinner", false);
         var selectedVal=component.get('v.SelectedPicklistVal');
         helper.FindCorporateMemberHelper(component, event, selectedVal);
    },
    disableFields : function(component, event, helper) {
        var val = component.find("alternateContact").get("v.value");
        if(val){
             component.set('v.isDisabled' , true);
        }else{
             component.set('v.isDisabled' , false);
        }
	},
     BillingContact : function(component, event, helper) {
        
         var corprateAcc = component.get("v.existingCorporateAccount");
        helper.BillingContacthelper(component, event, corprateAcc); 
    },
     legalContact : function(component, event, helper) {
        
         var corprateAcc = component.get("v.existingCorporateAccount");
        helper.legalContacthelper(component, event, corprateAcc); 
    },
   
     OnSave: function(component, event, helper) { 
         var assMemberTypeId=component.get('v.assMemberTypeId');
       // alert(assMemberTypeId);
         var isvalid=true;
         if(assMemberTypeId==null){
             isvalid=false;
             component.set("v.messageMemberType", "You need to select Memeber Type.");
            }	    
            if(isvalid == true){ 
              component.set("v.messageMemberType", "");
              helper.OnSaveHelper(component, event);
             
            }
       
    },
    OnSaveCorporateMember: function(component, event, helper) { 
        var selectVal=component.get('v.value');
                var firstNameId = component.find("firstName");
                var firstNameVal = firstNameId.get("v.value");
                var lastNameId = component.find("lastName");
                var lastNameVal = lastNameId.get("v.value");
                var emailId = component.find("emailId");
                var emailVal = emailId.get("v.value");
                var pcContactTypeId = component.find("pcContactTypeId");
               var pcContactTypeVal = pcContactTypeId.get("v.value");
                var isvalid = true;
                if(firstNameVal == ''){
                    isvalid=false;
                    firstNameId.setCustomValidity("You need to Enter First Name");
                    firstNameId.reportValidity();
                }else{
                    firstNameId.setCustomValidity("");
                    firstNameId.reportValidity();
                }
                if(lastNameVal == ''){
                    isvalid=false;
                    lastNameId.setCustomValidity("You need to Enter Last Name");
                    lastNameId.reportValidity();
                }else{
                    lastNameId.setCustomValidity("");
                    lastNameId.reportValidity();
                }
                if(emailVal == ''){
                    isvalid=false;
                    emailId.setCustomValidity("You need to Enter Email");
                    emailId.reportValidity();
                }else{
                    emailId.setCustomValidity("");
                    emailId.reportValidity();
                }
              if(pcContactTypeVal==''){
                isvalid=false;
                 component.set("v.contactTypemessage", "You need to select Contact Type.");
               }	
             
            if(isvalid == true){  
                helper.OnSaveCorporateMemberHelper(component, event);
            }    
       },  
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        //component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
       hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
       // component.set("v.spinner", false);
    },
    formatCorporatePhone: function(component, helper, event) {
        var phoneNo = component.find("CorporatePhone");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
    primaryContactPhone :function(component, helper, event) {
        var phoneNo = component.find("primaryContactPhone");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
    primaryContactMobile: function(component, helper, event) {
        var phoneNo = component.find("primaryContactMobile");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
    alternateContactPhone :function(component, helper, event) {
        var phoneNo = component.find("alternateContactPhone");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
   alternateContactMobile :function(component, helper, event) {
        var phoneNo = component.find("alternateContactMobile");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
    },
    billingContactPhone : function(component, helper, event) {
            var phoneNo = component.find("billingContactPhone");
            var phoneNumber = phoneNo.get('v.value');
            var s = (""+phoneNumber).replace(/\D/g, '');
            var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
            var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
            phoneNo.set('v.value',formattedPhone);
        },
     billingContactMobile : function(component, helper, event) {
            var phoneNo = component.find("billingContactMobile");
            var phoneNumber = phoneNo.get('v.value');
            var s = (""+phoneNumber).replace(/\D/g, '');
            var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
            var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
            phoneNo.set('v.value',formattedPhone);
        },
     legalContactPhone : function(component, helper, event) {
            var phoneNo = component.find("legalContactPhone");
            var phoneNumber = phoneNo.get('v.value');
            var s = (""+phoneNumber).replace(/\D/g, '');
            var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
            var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
            phoneNo.set('v.value',formattedPhone);
        },
     legalContactMobile :function(component, helper, event) {
            var phoneNo = component.find("legalContactMobile");
            var phoneNumber = phoneNo.get('v.value');
            var s = (""+phoneNumber).replace(/\D/g, '');
            var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
            var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
            phoneNo.set('v.value',formattedPhone);
        },
    onCheckboxChange : function(component, event, helper) {
		var availableCheckboxes = component.find('rowSelectionCheckboxId');
        var resetCheckboxValue  = false;
        if (Array.isArray(availableCheckboxes)) {
            availableCheckboxes.forEach(function(checkbox) {
                checkbox.set('v.value', resetCheckboxValue);
            }); 
        } else {
            availableCheckboxes.set('v.value', resetCheckboxValue);
        }
        event.getSource().set("v.value",true);
        var memberId = event.getSource().get("v.text");
        //alert(memberId);
        component.set("v.assMemberTypeId",memberId);

	},
    onCheckboxChangeCorporateAcct :function(component, event, helper) {
		var availableCheckboxes = component.find('corporatAcctSelectionCheckboxId');
        var resetCheckboxValue  = false;
        if (Array.isArray(availableCheckboxes)) {
            availableCheckboxes.forEach(function(checkbox) {
                checkbox.set('v.value', resetCheckboxValue);
            }); 
        } else {
            availableCheckboxes.set('v.value', resetCheckboxValue);
        }
        event.getSource().set("v.value",true);
        var corporateAccId = event.getSource().get("v.text");
        component.set("v.existingCorporateAccount",corporateAccId);
	},
     onCheckboxChangeCorporateMember :function(component, event, helper) {
		var availableCheckboxes = component.find('selectCorporateMemCheckboxId');
        var resetCheckboxValue  = false;
        if (Array.isArray(availableCheckboxes)) {
            availableCheckboxes.forEach(function(checkbox) {
                checkbox.set('v.value', resetCheckboxValue);
            }); 
        } else {
            availableCheckboxes.set('v.value', resetCheckboxValue);
        }
        event.getSource().set("v.value",true);
        var corporateMemberId = event.getSource().get("v.text");
        component.set("v.AssCorporateMemberId",corporateMemberId);
	},
    })