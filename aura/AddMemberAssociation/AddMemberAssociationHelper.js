({ 
    getAssCorporateMember : function(component, event,selectedVal) {
       var associationId = component.get("v.recordId");
        var action = component.get("c.getAssCorporateMember");
        action.setParams({associationId:associationId,selectVal:selectedVal});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue(); 
                //alert(result)
                 if(result.length > 0){  
                     component.set('v.corporateMemberList', result);
                     component.set("v.bNoRecordsCorpoMember" , false);
                 }else{
                    component.set("v.bNoRecordsCorpoMember" , true);
                 }
            }
        });
        $A.enqueueAction(action);
    },
    getAssMemberTypehelper : function(component, event) {
        var selectVal=component.get('v.value');
        var associationId = component.get("v.recordId");
        var memberRecordtype='';
        if(selectVal=="option1"){
            memberRecordtype = 'Corporate Member';
        }else if(selectVal=="option2"){
           memberRecordtype = 'Individual Member'; 
        }else if(selectVal=="option3"){
           memberRecordtype = 'Staff'; 
        }
       // alert(memberRecordtype);
        var action = component.get("c.getAssMemberType");
        action.setParams({associationId:associationId,memberRecordtypeName:memberRecordtype});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue(); 
              if(result.length > 0){  
                 component.set('v.memberTypeList', result);
                 component.set("v.bNoRecordsFoundMemberTyp" , false);
              }else{
                  component.set("v.bNoRecordsFoundMemberTyp" , true);
              }
            }
        });
        $A.enqueueAction(action);
    },
   getBaseUrl: function(component, event) {  
    var action = component.get('c.getBaseUrl')
          action.setCallback(this, function (response) {
            var state = response.getState()
            if (component.isValid() && state === 'SUCCESS') {
              var result = response.getReturnValue();
                //alert(result);
              component.set('v.baseUrl', result)
            }
          });
       $A.enqueueAction(action);
    },
    getUITheme: function(component, event) {
    var action = component.get('c.getUIThemeDescription')
          action.setCallback(this, function (response) {
            var state = response.getState()
            if (component.isValid() && state === 'SUCCESS') {
              var result = response.getReturnValue();
               // alert(result);
              component.set('v.theme_ui', result)
            }
          });
          $A.enqueueAction(action);
    },
    FindDomainHelper: function(component, event ,domainVal) {
    
       var domainVal=domainVal;
        // alert(domainVal);
        var action = component.get("c.fetchDomainCorporateAccount");
        action.setParams({domain:domainVal});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
               
                if(result.length > 0){    
                    component.set("v.existingCorporatAcctList", result);
                    component.set("v.bNoRecordsFound" , false);
                }else{
                     component.set("v.bNoRecordsFound" , true);
                     component.set("v.existingCorporatAcctList", null);
                }
            }
        });
        $A.enqueueAction(action);
    },
    FindCorporateMemberHelper: function(component, event ,selectedVal) {
    
        var selectedVal=selectedVal;
        //alert(memberVal);
        var associationId = component.get("v.recordId");
        var memberVal=component.get('v.corporateMemberName');
        var action = component.get("c.getAssCorporateMember");
        action.setParams({associationId:associationId,selectVal:selectedVal,memberVal:memberVal});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue(); 
                 if(result.length > 0){  
                     component.set('v.corporateMemberList', result);
                     component.set("v.bNoRecordsCorpoMember" , false);
                 }else{
                      component.set("v.bNoRecordsCorpoMember" , true);
                      component.set('v.corporateMemberList', null);
                 }
            }
        });
        $A.enqueueAction(action);
    },
    BillingContacthelper : function(component, event, corprateAcc) {
        var selectVal = component.get("v.billingContact");
        var corprateAcc = component.get("v.existingCorporateAccount");
        //alert(selectVal);
        //alert(corprateAcc);
        if(selectVal == "Other" && corprateAcc != null){
            component.set('v.existiCorporatAccount',true);
            component.set('v.corporatePersonAccountList', null);
            var action = component.get("c.fetchCorporatePersonAccount");
            action.setParams({corprateAccId:corprateAcc});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS"){
                    var oRes = response.getReturnValue();
                        if(oRes.length > 0){
                            component.set('v.corporatePersonAccountList', oRes);
                            //alert(component.get('v.corporatePersonAccountList'));
                         }
               }else{
                    alert('Error...');
                }
            });
            $A.enqueueAction(action);
        }else{
            component.set('v.corporatePersonAccountList', null);
            component.set('v.existiCorporatAccount',false);
            component.set('v.newBillContact',false);
        }
    },
     legalContacthelper : function(component, event, corprateAcc) {
        var selectVal = component.get("v.legalContact");
        var corprateAcc = component.get("v.existingCorporateAccount");
       // alert(selectVal);
       // alert(corprateAcc);
        if(selectVal == "Other" && corprateAcc != null){
            component.set('v.existiCorporatAccount',true);
            component.set('v.corporatePersonAccountList', null);
            var action = component.get("c.fetchCorporatePersonAccount");
            action.setParams({corprateAccId:corprateAcc});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS"){
                    var oRes = response.getReturnValue();
                        if(oRes.length > 0){
                            component.set('v.corporatePersonAccountList', oRes);
                            //alert(component.get('v.corporatePersonAccountList'));
                         }
               }else{
                    alert('Error...');
                }
            });
            $A.enqueueAction(action);
        }else{
            component.set('v.corporatePersonAccountList', null);
              component.set('v.existiCorporatAccount',false);
               component.set('v.newLegalContact',false);
        }
    },
    OnSaveHelper : function(component, event, domainVal){
       // alert('Save');
        var selectVal=component.get('v.value');
        var billselectVal = component.get("v.billingContact");
        var legalselectval=component.get("v.legalContact");
        var billcontPresonAccId=component.get("v.billContPersonAccountId");
        var legalContPersonAccountId=component.get("v.legalContPersonAccountId");
        var existingCorporateAccVal = component.get("v.existingCorporateAccount");
        var associationId = component.get("v.recordId");
        var domainVal=component.get('v.domainName');
        var accuntval=component.get('v.existingCorporateAccount');
        var corporateAccountVal=component.get('v.corporateAccount');
        var primaryContactVal=component.get('v.primaryContact');
        var alternateContactVal=component.get('v.alternateContactObj');
        var billingContactObjVal=component.get('v.billingContactObj');
        var legalContactObjVal=component.get('v.legalContactObj');        
        var assMemberTypeId=component.get('v.assMemberTypeId');
        var createNewBillContact=component.get('v.newBillContact');
        var createNewLegalContact=component.get('v.newLegalContact');
        
      
        //if(selectVal)
       //alert('existingCorporateAccVal1 '+existingCorporateAccVal);
      // alert('billcontPresonAccId '+billcontPresonAccId);
       //alert('legalContPersonAccountId '+legalContPersonAccountId);
        if(selectVal=="option1"){
             if(existingCorporateAccVal==null){  
                  component.set("v.spinner", true);
                    var action = component.get("c.insertNewCorporateAccount");
                    action.setParams({corporateAccount:corporateAccountVal,
                                      corporateMember:corporateAccountVal,
                                      personAccount:primaryContactVal ,
                                      individualMember: primaryContactVal,
                                      alternatePersonAccount:alternateContactVal,
                                      alternateIndividualMember:alternateContactVal,
                                      billingPersonAccount:billingContactObjVal,
                                      billingIndividualMember:billingContactObjVal,
                                      legalPersonAccount:legalContactObjVal,
                                      legalIndividualMember:legalContactObjVal,
                                      assMemberType:assMemberTypeId,
                                      associationId:associationId,
                                      existingCorporateAccId:existingCorporateAccVal,
                                      billselectVal:billselectVal,
                                      legalselectval:legalselectval,
                                      createNewBillContact:createNewBillContact,
                                      createNewLegalContact:createNewLegalContact
                                      });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        
                        if (state === "SUCCESS"){
                            var oRes = response.getReturnValue();
                            component.set("v.newCorporateMemberId", oRes);
                            
                            var newCorporateMemberId=component.get('v.newCorporateMemberId');
                            var basUrl=component.get('v.baseUrl');
                                 component.set("v.spinner", false);
                                 if(component.get('v.theme_ui') === 'Theme4d') {
                                    var lightUrl= basUrl.replace('my.salesforce.com','lightning.force.com/lightning/r/Contact');
                                     window.location.href = '/lightning/r/Contact/'+newCorporateMemberId+ '/view'; 
                                    
                                      var toastEvent = $A.get("e.force:showToast");
                                        toastEvent.setParams({
                                            title : 'Success Message',
                                            message: 'Create Corporate Member Successfully',
                                            messageTemplate: 'Record {0} created! See it {1}!',
                                            duration:' 30000',
                                            key: 'info_alt',
                                            type: 'success',
                                            mode: 'pester'
                                        });
                                        toastEvent.fire();
                                }
                                
                       }else{
                            alert('Error...');
                        }
                    });
                    $A.enqueueAction(action);
            }
            if(existingCorporateAccVal!=null){
                //alert('legalContPersonAccountId '+legalContPersonAccountId);
                    component.set("v.spinner", true);
            		var action = component.get("c.existingCorporateAccount");
                    action.setParams({personAccount:primaryContactVal ,
                                      individualMember: primaryContactVal,
                                      existingCorporateAccId:existingCorporateAccVal,
                                      alternatePersonAccount:alternateContactVal,
                                      alternateIndividualMember:alternateContactVal,
                                      billingPersonAccount:billingContactObjVal,
                                      billingIndividualMember:billingContactObjVal,
                                      legalPersonAccount:legalContactObjVal,
                                      legalIndividualMember:legalContactObjVal,
                                      assMemberType:assMemberTypeId,
                                      associationId:associationId,
                                      billcontPresonAccId:billcontPresonAccId,
                                      legalcontpersonAccId:legalContPersonAccountId,
                                      billselectVal:billselectVal,
                                      legalselectval:legalselectval,
                                      createNewBillContact:createNewBillContact,
                                      createNewLegalContact:createNewLegalContact
                                     });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS"){
                            var oRes = response.getReturnValue();
                             component.set("v.newCorporateMemberId", oRes);
                                var newCorporateMemberId=component.get('v.newCorporateMemberId');
                                var basUrl=component.get('v.baseUrl');
                                 component.set("v.spinner", false);
                                     if (component.get('v.theme_ui') === 'Theme4d') {
                                        var lightUrl= basUrl.replace('my.salesforce.com','lightning.force.com/lightning/r/Contact');
                                         window.location.href = '/lightning/r/Contact/'+newCorporateMemberId+ '/view'; 
                                        
                                          var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                title : 'Success Message',
                                                message: 'Create Corporate Member Successfully',
                                                messageTemplate: 'Record {0} created! See it {1}!',
                                                duration:' 30000',
                                                key: 'info_alt',
                                                type: 'success',
                                                mode: 'pester'
                                            });
                                            toastEvent.fire();
                                 }   
                       }else{
                            alert('Error...');
                        }
                    });
                    $A.enqueueAction(action);
            }
    }
    if(selectVal=="option2"){
        component.set("v.spinner", true);
        var action = component.get("c.insertNewIndividualMember");
                    action.setParams({personAccount:primaryContactVal ,
                                      individualMember: primaryContactVal,
                                      assMemberType:assMemberTypeId,
                                      associationId:associationId
                                      });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS"){
                            var oRes = response.getReturnValue();
                            component.set("v.newindivMemberId", oRes);
                             var newindivMemberId=component.get('v.newindivMemberId');
                             var basUrl=component.get('v.baseUrl');
                              component.set("v.spinner", false);
                                 if (component.get('v.theme_ui') === 'Theme4d') {
                                    var lightUrl= basUrl.replace('my.salesforce.com','lightning.force.com/lightning/r/Contact');
                                     window.location.href = '/lightning/r/Contact/'+newindivMemberId+ '/view'; 
                                    
                                      var toastEvent = $A.get("e.force:showToast");
                                        toastEvent.setParams({
                                            title : 'Success Message',
                                            message: 'Create Individual Member Successfully',
                                            messageTemplate: 'Record {0} created! See it {1}!',
                                            duration:' 30000',
                                            key: 'info_alt',
                                            type: 'success',
                                            mode: 'pester'
                                        });
                                        toastEvent.fire();
                                }   
                       }else{
                            alert('Error...');
                        }
                    });
                    $A.enqueueAction(action);     
     }
	 if(selectVal=="option3"){
        component.set("v.spinner", true);
        var action = component.get("c.insertNewStaffMember");
                    action.setParams({personAccount:primaryContactVal ,
                                      individualMember: primaryContactVal,
                                      assMemberType:assMemberTypeId,
                                      associationId:associationId
                                      });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS"){
                            var oRes = response.getReturnValue();
                            component.set("v.newStaffMemberId", oRes);
                             var newStaffMemberId=component.get('v.newStaffMemberId');
                             var basUrl=component.get('v.baseUrl');
                           		component.set("v.spinner", false);
                                 if (component.get('v.theme_ui') === 'Theme4d') {
                                    var lightUrl= basUrl.replace('my.salesforce.com','lightning.force.com/lightning/r/Contact');
                                     window.location.href = '/lightning/r/Contact/'+newStaffMemberId+ '/view'; 
                                    
                                      var toastEvent = $A.get("e.force:showToast");
                                        toastEvent.setParams({
                                            title : 'Success Message',
                                            message: 'Create Staff Member Successfully',
                                            messageTemplate: 'Record {0} created! See it {1}!',
                                            duration:' 30000',
                                            key: 'info_alt',
                                            type: 'success',
                                            mode: 'pester'
                                        });
                                        toastEvent.fire();
                                }   
                       }else{
                            alert('Error...');
                        }
                    });
                    $A.enqueueAction(action);     
     }
   
    },
     OnSaveCorporateMemberHelper : function(component, event, domainVal){
       var primaryContactMemberCategory =component.set("v.primaryContactMemberCategory");
       var AssCorporateMemberId= component.get('v.AssCorporateMemberId'); 
       var primaryContactVal=component.get('v.primaryContact'); 
       var associationId = component.get("v.recordId");
            component.set("v.spinner", true); 
     		var action = component.get("c.insertNewCorporateRepresentative");
                    action.setParams({personAccount:primaryContactVal ,
                                      corporateRepresentative: primaryContactVal,
                                      assCorporateMemberId:AssCorporateMemberId,
                                      associationId:associationId
                                      //memberCategory:primaryContactMemberCategory,
                                      });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS"){
                            var oRes = response.getReturnValue();
                            component.set("v.newCorporateRepresentativeId", oRes);
                             var newRepresnetMemberId=component.get('v.newCorporateRepresentativeId');
                             var basUrl=component.get('v.baseUrl');
                                component.set("v.spinner", false);
                                 if (component.get('v.theme_ui') === 'Theme4d') {
                                    var lightUrl= basUrl.replace('my.salesforce.com','lightning.force.com/lightning/r/Contact');
                                     window.location.href = '/lightning/r/Contact/'+newRepresnetMemberId+ '/view'; 
                                    
                                      var toastEvent = $A.get("e.force:showToast");
                                        toastEvent.setParams({
                                            title : 'Success Message',
                                            message: 'Create Corporate Representative Successfully',
                                            messageTemplate: 'Record {0} created! See it {1}!',
                                            duration:' 30000',
                                            key: 'info_alt',
                                            type: 'success',
                                            mode: 'pester'
                                        });
                                        toastEvent.fire();
                                }   
                       }else{
                            alert('Error...');
                        }
                    });
                    $A.enqueueAction(action);     
     },   
})