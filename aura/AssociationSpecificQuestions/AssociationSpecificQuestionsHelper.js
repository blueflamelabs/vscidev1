({
    getMasterQuesData : function(component,event,helper){
        //Load the Account data from apex
        var action = component.get("c.getMasterQuestionRec");
        var toastReference = $A.get("e.force:showToast");
        action.setCallback(this,function(response){
            if(response.getState() === "SUCCESS"){
                var questWrapper = response.getReturnValue();
                var setOfAddedQuestion = component.get("v.setOfAddedQuestion");
                var questWrapperList = [];
                var questWrapperMap = [];
                var questWrapperMap = {};
                for(var x = 0;x < questWrapper.length ; x++){
                    if(!setOfAddedQuestion.includes(questWrapper[x].masterQuest.Id)){
                        questWrapperList.push(questWrapper[x]);
                    }
                    questWrapperMap[questWrapper[x].masterQuest.Id] = questWrapper[x];
                }
                var masterQuestionAns = component.get("v.wrapMasterAssociationQuestDataList");
                for(var x = 0;x < masterQuestionAns.length;x++){
                    console.log(questWrapperMap[masterQuestionAns[x].masterQuestAns.Master_Question__c]);
                    var masterQuestionAnsValue = questWrapperMap[masterQuestionAns[x].masterQuestAns.Master_Question__c].masterQuestAnsValue;
                    for(var z = 0;z < masterQuestionAnsValue.length;z++){
                        if(masterQuestionAns[x].associationQuestAnsValueList.length > 0){
                            var notmatch = true;
                            for(var y = 0;y < masterQuestionAns[x].associationQuestAnsValueList.length;y++){
                                if(masterQuestionAnsValue[z].Value__c == masterQuestionAns[x].associationQuestAnsValueList[y].value ){
                                    notmatch = false 
                                }
                            }
                            if(notmatch){
                                var MasterQuestionAnswerValueWrapper = {};
                                MasterQuestionAnswerValueWrapper.value = masterQuestionAnsValue[z].Value__c;
                                MasterQuestionAnswerValueWrapper.isSelected = false;
                                MasterQuestionAnswerValueWrapper.associationQuestAnsValue = {};
                                MasterQuestionAnswerValueWrapper.associationQuestAnsValue.Master_Question_Answer_Value__c = masterQuestionAnsValue[z].Id;
                                masterQuestionAns[x].associationQuestAnsValueList.splice(z,0,MasterQuestionAnswerValueWrapper);
                            }
                        }else{
                            var MasterQuestionAnswerValueWrapper = {};
                            MasterQuestionAnswerValueWrapper.value = masterQuestionAnsValue[z].Value__c;
                            MasterQuestionAnswerValueWrapper.isSelected = false;
                            MasterQuestionAnswerValueWrapper.associationQuestAnsValue = {};
                            MasterQuestionAnswerValueWrapper.associationQuestAnsValue.Master_Question_Answer_Value__c = masterQuestionAnsValue[z].Id;
                            masterQuestionAns[x].associationQuestAnsValueList.splice(z,0,MasterQuestionAnswerValueWrapper);
                        }
                    }
                }
                //console.log('----',questWrapperList);
                component.set("v.wrapMasterAssociationQuestDataList",masterQuestionAns);
                component.set("v.masterMainQuestWrapperList",questWrapperList);
                component.set("v.masterMainQuestWrapperListTemp",questWrapperList);
                component.set("v.startPosn",0);
                component.set("v.endPosn",component.get("v.pageSize")-1);
                this.sortBy(component,event,helper);
                helper.buildData1(component);
            }else{
                alert('hi');
            }
        });
        $A.enqueueAction(action);
    },
    fetchCategoryPicklist : function(component, event, helper){
        var action = component.get("c.getPicklistvalues");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.CategoryPicklist", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    searchKeyChange : function(component, event, helper){
        var searchKeyword = component.get("v.searchKeyword");
        var selectedPickVal = component.get("v.categoryValue");
        var searchList = component.get("v.masterMainQuestWrapperListTemp");
        var searchListFinalCat = [];
        var searchListFinal = [];
        
        if(selectedPickVal != ''){
            if(selectedPickVal != 'All'){
                for(var x  = 0 ;x < searchList.length;x++){
                    if(searchList[x].masterQuest.Category__c.toLowerCase().indexOf(selectedPickVal.toLowerCase()) != -1){
                        searchListFinalCat.push(searchList[x]);
                    }
                }
            }else{
                searchListFinalCat = searchList;
            }
        }else{
            searchListFinalCat = searchList;
        }
        
        if(searchKeyword != ''){
            for(var x  = 0 ;x < searchListFinalCat.length;x++){
                if(searchListFinalCat[x].masterQuest.Master_Question__c != undefined && searchListFinalCat[x].masterQuest.Master_Question__c.toLowerCase().indexOf(searchKeyword.toLowerCase()) != -1){
                    searchListFinal.push(searchListFinalCat[x]);
                }else if(searchListFinalCat[x].masterQuest.Category__c != undefined && searchListFinalCat[x].masterQuest.Category__c.toLowerCase().indexOf(searchKeyword.toLowerCase()) != -1){
                    searchListFinal.push(searchListFinalCat[x]);
                }else if(searchListFinalCat[x].masterQuest.Field_Data_Type__c != undefined && searchListFinalCat[x].masterQuest.Field_Data_Type__c.toLowerCase().indexOf(searchKeyword.toLowerCase()) != -1){
                    searchListFinal.push(searchListFinalCat[x]);
                }
            }
            component.set("v.masterMainQuestWrapperList",searchListFinal);
        }else{
            component.set("v.masterMainQuestWrapperList",searchListFinalCat);
        }
        helper.buildData(component);
    },
    buildData : function(component) {
        var data = [];
        var pageNumber = component.get("v.currentPageNumber");
        var pageSize = component.get("v.pageSize");
        var allWrapperData = component.get("v.masterMainQuestWrapperList");
        component.set("v.totalPages", Math.ceil((allWrapperData.length)/component.get("v.pageSize")));
        var x = (pageNumber-1) * pageSize;
        var y = x + pageSize;
        for(;x<y;x++){
            if(allWrapperData[x])
                data.push(allWrapperData[x]);
        }
        component.set("v.masterQuestWrapperList", data);
    },
    getAssociationRelatedAssQuest : function(cmp, event, helper) {
        console.log('inside related Ass Quest ');
        var action = cmp.get("c.getAssociationQuestionRec");
        action.setParams({
            "associationId" : cmp.get('v.recordId')
        });
        action.setCallback(this, function(a) {
            var associationResponse = a.getReturnValue();
            console.log();
            if(a.getState() === "SUCCESS"){
                cmp.set("v.wrapMasterAssociationQuestDataList", associationResponse);
                var setOfAddedQuestion = [];
                for(var x = 0;x < associationResponse.length;x++){
                    setOfAddedQuestion.push(associationResponse[x].masterQuestAns.Master_Question__c);
                    associationResponse[x].masterQuestAns.Association_Question_Answer_Values__r = null;
                    associationResponse[x].masterQuestAns.Member_Answers__r = null;
                }
                cmp.set("v.setOfAddedQuestion",setOfAddedQuestion);
                cmp.set("v.startPosn1",0);
                cmp.set("v.endPosn1",cmp.get("v.pageSize")-1);
                cmp.set("v.totalPages1", Math.ceil((associationResponse.length-1)/cmp.get("v.pageSize")));
                cmp.set("v.currentPageNumber1",1);
                this.getMasterQuesData(cmp, event, helper);
                this.buildData1(cmp);
            }else{
                alert('hi');
            }
        });
        $A.enqueueAction(action);
    },
    buildData1 : function(component) {
        var data = [];
        var pageNumber = component.get("v.currentPageNumber1");
        var pageSize = component.get("v.pageSize");
        var allWrapperData = component.get("v.wrapMasterAssociationQuestDataList");
        component.set("v.totalPages1", Math.ceil((allWrapperData.length)/component.get("v.pageSize")));
        var x = (pageNumber-1) * pageSize;
        var y = x + pageSize;
        for(;x<y;x++){
            if(allWrapperData[x]){
            	data.push(allWrapperData[x]);
            }
        }
        component.set("v.wrapAssociationQuestDataList",data);
    },
    saveAndClose : function(component,event, helper,actionType){
        
        var masterQuestionAns = component.get("v.wrapMasterAssociationQuestDataList");
        var masterQuestionAnsDel = component.get("v.wrapDeleteAssociationQuestDataList");
        console.log(JSON.stringify(masterQuestionAns));
        var action = component.get("c.saveMasterAns");
        action.setParams({
            "jsonString" : JSON.stringify(masterQuestionAns),
            'deleteJsonString': JSON.stringify(masterQuestionAnsDel)
        });
        action.setCallback(this, function(a) {
            
            if(actionType == 'Close' && a.getState() === "SUCCESS"){
                this.showDelMsgToastfire('Save successfully');
                setTimeout(function(){$A.get("e.force:closeQuickAction").fire();},1);
                
            }else if(actionType == 'Continue' && a.getState() === "SUCCESS"){
                this.showDelMsgToastfire('Save successfully');
                this.getAssociationRelatedAssQuest(component,event,helper);
                component.set("v.wrapDeleteAssociationQuestDataList",[]);
            }else{
                alert('error');
            }
            
        });
        $A.enqueueAction(action);
    },
    showDelMsgToastfire : function(msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: msg,
            duration:'3000',
            type: 'Success'
        });
        toastEvent.fire();
    },
    sort :  function(component,event,helper){
        
        let sortField = event.currentTarget.getAttribute('data-sortField'),
            title = event.currentTarget.getAttribute('title'),
            sortIndex = event.currentTarget.getAttribute('data-sortIndex'),
            sortProperties = component.get("v.sortProperties"); 
        var sortTitle = title.charAt(0).toUpperCase() ;
        var lowrstr =  title.substr(1,title.length) ;
        
        if(sortProperties.sortField == sortField) {
            if(sortProperties.sortOrder == 'asc') {
                sortProperties.sortOrder = 'desc';
                
            } else {
                sortProperties.sortOrder = 'asc';
            }
        } else {
            sortProperties.sortField = sortField;
            sortProperties.sortOrder = 'desc';
        }
        component.set("v.sortProperties",sortProperties);
        helper.sortBy(component,event, helper);
    },
    sortBy : function(component,event, helper){
        var  masterecords = component.get("v.masterMainQuestWrapperListTemp");
        var masterList = [];
        //var mapMasterAssValue = new Map();
        for(var i=0; i< masterecords.length; i++){
            masterList.push( masterecords[i]);
        }
        //console.log('--masterecords--',masterList);
        let sortProperties = component.get("v.sortProperties"),
            records = masterList,
            sortAsc = sortProperties.sortOrder == 'asc' ? true : false,
            field = sortProperties.sortField;
         records.sort(function(a,b){
                let avalue = (a.masterQuest[field] != 0 && a.masterQuest[field])  ? a.masterQuest[field] : null;
                let bvalue = (b.masterQuest[field] != 0 && b.masterQuest[field]) ? b.masterQuest[field] : null;
                
                if(avalue == "" || avalue == null) return 1;
                if(bvalue == "" || bvalue == null) return -1;
                alert(avalue +' !!! '+bvalue);
                let t1 = avalue == bvalue ,
                    t2 = (avalue != 0 && !avalue && bvalue) || (avalue < bvalue);
                return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
            });
        component.set("v.currentPageNumber",1);
        component.set("v.masterMainQuestWrapperListTemp",records);
        this.searchKeyChange(component,event, helper);
    },
    setColumnWrapper: function(component,event, helper) {
        const columns = [
            {
                label : 'Master Question',
                apiName : 'Master_Question__c',
                type : 'text',
                sortable : true
            },
            {
                label : 'Category',
                apiName : 'Category__c',
                type : 'picklist',
                sortable : true
            },
            {
                label : 'Field Data Type',
                apiName : 'Field_Data_Type__c',
                type : 'PickList',
                sortable : true
            },
            {
                label : 'Last Modified Date',
                apiName : 'LastModifiedBy.LastModifiedDate',
                type : 'DateTime',
                sortable : true
            },];
            component.set("v.columns",columns);
     },
 })