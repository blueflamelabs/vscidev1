@IsTest
public class AssociationQuestionController_Test{
    @IsTest
       static void AssociationQuestionsCTRLTestMethod(){
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
            Association_Question__c assque = new Association_Question__c();
            assque.Association__c = ass.id;
            assque.Displayed_Question__c = 'test';
            insert assque;
            
            Master_Question__c objMasterQu = new Master_Question__c();
            objMasterQu.Master_Question__c = 'test';
            objMasterQu.Field_Data_Type__c = 'Multi-Select';
            objMasterQu.Category__c = 'Demographics';
            objMasterQu.Status__c = 'Published';
            insert objMasterQu;
            
            Master_Question_Answer_Values__c objMasQuAns = new Master_Question_Answer_Values__c();
            objMasQuAns.Value__c = 'English';
            objMasQuAns.Active__c = false;
            objMasQuAns.Master_Question__c = objMasterQu.id;
            insert objMasQuAns;
            
            Association_Question_Answer_Value__c objAssQueAns = new Association_Question_Answer_Value__c();
            objAssQueAns.Association_Question__c = assque.Id;
            objAssQueAns.Master_Question_Answer_Value__c = objMasQuAns.Id;
            insert objAssQueAns;
            
            List<Association_Question_Answer_Value__c> objQuAnsValue = new List<Association_Question_Answer_Value__c>();
            objQuAnsValue.add(objAssQueAns);
            AssociationQuestionController.getAssociationQuestionRec(ass.id);
            AssociationQuestionController.getMasterQuestionRec();
            AssociationQuestionController.getPicklistvalues();
            List<String> objDataType = new List<String>();
            objDataType.add('Integer');
            
            AssociationQuestionController.MasterQuestionAnswerValueWrapper objAnsValue = new AssociationQuestionController.MasterQuestionAnswerValueWrapper();
            objAnsValue.associationQuestAnsValue = objAssQueAns;
            objAnsValue.value = 'test';
            objAnsValue.isSelected = true;
            
            AssociationQuestionController.MasterQuestAnsWrapper objMasQu = new AssociationQuestionController.MasterQuestAnsWrapper();
            objMasQu.dataTypeValue = objDataType;
            objMasQu.masterQuestAns = assque;
            objMasQu.checkAccordion = true;
            objMasQu.checkEye = true;
            objMasQu.isDeleteIcon = true;
            objMasQu.isDelete = true;
            objMasQu.associationQuestAnsValueList = new List<AssociationQuestionController.MasterQuestionAnswerValueWrapper>{objAnsValue};
            objMasQu.isAssAnsValue = true;
            
            List<AssociationQuestionController.MasterQuestAnsWrapper> objMasterAns = new List<AssociationQuestionController.MasterQuestAnsWrapper>();
            objMasterAns.add(objMasQu);
            AssociationQuestionController.saveMasterAns(Json.serialize(objMasterAns),Json.serialize(objMasterAns));
       }   
 }