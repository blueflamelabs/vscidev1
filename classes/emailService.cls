global class emailService implements Messaging.InboundEmailHandler {
	  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		  Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
          
          system.debug(email.toAddresses);
          system.debug(email.subject);
          
          String record = [SELECT Record_Id__c, To_Email__c FROM Record_Email__mdt WHERE To_Email__c = :email.toAddresses].Record_Id__c;

          system.debug(record);

          String subjUser = [SELECT Id, Email FROM User WHERE Email = :email.subject].Id;

            system.debug(subjUser);
    
        
    
        FeedItem post = new FeedItem();
        post.ParentId = record;
        post.Body = email.plainTextBody;
        post.createdById = subjUser;
    
        
        
    
       
        try {
        
        insert post;
       
        }
       
    catch (Exception e) {
        System.debug('exception: ' + e);
    }
    
    // Set the result to true. No need to send an email back to the user 
    // with an error message
    result.success = true;
    
    // Return the result for the Apex Email Service
    return result;
      }
  }