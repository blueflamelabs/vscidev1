/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*     1.0        240719         VennScience_BFL_Amruta       This Scheduler is built to execute the 'NotifyIndividualMembersBatch' batch class
**********************************************************************************************************************************************************/
global class NotifyIndividualMembersScheduler implements Schedulable {
    /**
    * Method Name : scheduleBatchForEveryMidnight
    * Parameters  : 
    * Description : Used to call the scheduler and schedule the batch class using cron expression
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    global void scheduleBatchForEveryMidnight() {
        String SCHEDULE_JOB_NAME = 'NotifyIndividualMembers Batch';
        Set<String> jobState = new Set<String>{'COMPLETE', 'ERROR', 'DELETED'};
        Boolean isJobRunning = false;
        for(CronTrigger ct :[select Id, State from CronTrigger where CronJobDetail.Name = :SCHEDULE_JOB_NAME]){
            if(jobState.contains(ct.State) || Test.isRunningTest()) {
                System.abortJob(ct.Id);                        
            }
            else {
                isJobRunning = true;                         
            }
        }
       if(!isJobRunning){
           String cronExp = '0 0 0 * * ?';
           String jobID = system.schedule('NotifyIndividualMembers Batch', cronExp, new NotifyIndividualMembersScheduler());
       }
        
    }
    /**
    * Method Name : execute
    * Parameters  : param1: SchedulableContext 
    * Description : Scheduler interface method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    global void execute(SchedulableContext ctx) {
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch(); 
        database.executebatch(objBatch);
    }
}