public without sharing class AssociationQuestionsController {
    
    
    @AuraEnabled
    public List<Association_Question__c> associationQuestReturnList {get; set;}
    
    @AuraEnabled
    public static List<QuestionsWrapper> getMasterQuestionRec1() {
        List<QuestionsWrapper> questWrapperList = new List<QuestionsWrapper>();
        
        //try{ 
        for(Master_Question__c masterQObj : [SELECT Id,
                                             Master_Question__c,
                                             Category__c,
                                             Field_Data_Type__c,
                                             LastModifiedBy.LastModifiedDate,
                                             (SELECT Id,
                                              Name,Value__c
                                              FROM Question_Answer_Values__r),
                                             (SELECT Id,Association__c
                                              FROM Association_Questions__r)
                                             FROM Master_Question__c 
                                             WHERE Status__c = 'Published']) {
                                                 QuestionsWrapper questWrapper = new QuestionsWrapper();
                                                 //System.debug('==FOR===masterQObj=====' + masterQObj);
                                                 questWrapper.checkAccordion = false;
                                                 questWrapper.checkChildAccordion = false;
                                                 //questWrapper.checkChildAdd = false;
                                                 //questWrapper.checkEye = false;
                                                 questWrapper.checkAdd = false;
                                                 questWrapper.success = true;
                                                 List<Master_Question__c> tempMasterQList = new List<Master_Question__c>();
                                                 tempMasterQList =  questWrapper.masterQuestsList;
                                                 
                                                 if(masterQObj.Association_Questions__r.isEmpty()) {
                                                     //for(Association_Question__c associationQuest : masterQObj.Association_Questions__r) {
                                                         //if(associationQuest.Master_Question__c != null) {
                                                             tempMasterQList.add(masterQObj);
                                                             system.debug('tempMasterQList==='+tempMasterQList);
                                                             questWrapper.masterQuestsList = tempMasterQList;
                                                             
                                                         //}
                                                     //}
                                                     
                                                     system.debug('wrapflll==<<'+questWrapper.masterQuestsList);
                                                     //questWrapperList.add(questWrapper);
                                                     
                                                     // 08.08.2019
                                                     Map<Id,List<Master_Question_Answer_Values__c>> mapMasterQueIdVSMasterQueAnsList = new Map<Id,List<Master_Question_Answer_Values__c>>(); 
                                                     System.debug('=====masterQObj.Question_Answer_Values__r.size()=====' + masterQObj.Question_Answer_Values__r.size()); 
                                                     for(Master_Question_Answer_Values__c masterQuesAnsObj : masterQObj.Question_Answer_Values__r) {
                                                         //System.debug('=====masterQuesAnsObj=====' + masterQuesAnsObj); 
                                                         //questWrapper.mapOfMasterQuestAns.put(masterQObj.Id, value);
                                                         // 08.08.2019
                                                         if(!mapMasterQueIdVSMasterQueAnsList.containsKey(masterQObj.Id)) {
                                                             mapMasterQueIdVSMasterQueAnsList.put(masterQObj.Id,new List<Master_Question_Answer_Values__c>{masterQuesAnsObj});    
                                                         } else {
                                                             List<Master_Question_Answer_Values__c> listPreviousMasterQueAns = new List<Master_Question_Answer_Values__c>();  
                                                             listPreviousMasterQueAns.addAll(mapMasterQueIdVSMasterQueAnsList.get(masterQObj.Id));
                                                             listPreviousMasterQueAns.add(masterQuesAnsObj);
                                                             mapMasterQueIdVSMasterQueAnsList.put(masterQObj.Id,listPreviousMasterQueAns);
                                                         } // end of if-else block
                                                     } // End of inner for
                                                     //System.debug('mapMasterQueIdVSMasterQueAnsList========='+mapMasterQueIdVSMasterQueAnsList);
                                                     questWrapper.mapOfMasterQuestAns.putAll(mapMasterQueIdVSMasterQueAnsList);
                                                     questWrapperList.add(questWrapper);
                                                 } // End of outer for
                                             }
      
        System.debug('==questWrapperList=='+questWrapperList);
        return questWrapperList;
        
    }
    
    @AuraEnabled
    public static List<QuestionsWrapper> getMasterQuestionRec() { 
        List<QuestionsWrapper> questWrapperList = new List<QuestionsWrapper>();
        List<Master_Question__c> masterQuestList = new List<Master_Question__c>();
        Set<Id> setOfMAsterIds = new Set<Id>();
        QuestionsWrapper questWrapper = new QuestionsWrapper();
        try{ 
            
            masterQuestList  =     [SELECT Id,
                                    Master_Question__c,
                                    Category__c,
                                    Field_Data_Type__c,
                                    LastModifiedBy.LastModifiedDate
                                    FROM Master_Question__c 
                                    WHERE Status__c = 'Published'];
            for(Master_Question__c masterQObj : masterQuestList) {
                setOfMAsterIds.add(masterQObj.Id); 
            }
            
            List<Master_Question_Answer_Values__c> listOfmasterQAns = [SELECT Id,
                                                                       Name
                                                                       FROM Master_Question_Answer_Values__c
                                                                       WHERE Master_Question__c IN : setOfMAsterIds];
            
            questWrapper.masterQuestsList = masterQuestList;
            //questWrapper.masterQuestAnsList = listOfmasterQAns;
            questWrapper.checkAccordion = false;
            questWrapper.checkAdd = false;
            questWrapper.success = true;
            System.debug('==questWrapper.masterQuestsList==='+questWrapper.masterQuestsList);
            //System.debug('==questWrapper.masterQuestAnsList==='+questWrapper.masterQuestAnsList);
        }
        catch(Exception e){
            questWrapper.message = e.getMessage();
            questWrapper.success = false;
        }
        System.debug('==questWrapper=='+questWrapper);
        questWrapperList.add(questWrapper);
        System.debug('==questWrapperList=='+questWrapperList);
        return questWrapperList;
        
    }
    @AuraEnabled
    public static String UpdateAssociationQuest(List<Association_Question__c> associationWrapper, String associationRecId, List<String> associationAnsValList) {
        
        System.debug('---associationWrapper=='+associationWrapper);
        System.debug('---associationWrapper=='+associationAnsValList);
        if(!associationWrapper.isEmpty()) {
            Update associationWrapper;
        }
        Set<String> setofAssociationAnsIds = new Set<String>();
        setofAssociationAnsIds.addAll(associationAnsValList);
        System.debug('---setofAssociationAnsIds=='+setofAssociationAnsIds);
        if(!associationAnsValList.isEmpty()) {
            List<Association_Question_Answer_Value__c> listOfAssociatioAns = [SELECT Id
                                                                              FROM Association_Question_Answer_Value__c
                                                                              WHERE Id IN : setofAssociationAnsIds]; 
            Delete listOfAssociatioAns;
        }
        
        String successMsg = '';
        successMsg = 'Record Successfully Updated';
        
        return successMsg;
    }
    
    @AuraEnabled
    public static List<String> getPicklistvalues1() {
        
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Master_Question__c.Category__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        options.add('All');
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
        
        
    }
    
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    
    
    @AuraEnabled
    public static List<PickListWrapper> getPicklistvalues(String objectName, String field_apiname) {
        
        System.debug('--objectName-'+objectName+'---field_apiname--'+field_apiname);
        List<PickListWrapper> optionlist = new List<PickListWrapper>();
        
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap(); 
        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();
        
        optionlist.add(new PickListWrapper('Filter by Category','Filter by Category'));
        
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(new PickListWrapper(pv.getLabel(), pv.getValue()));
        }
        System.debug('==optionlist=='+optionlist);
        return optionlist;
    }
    
    @AuraEnabled
    public static List<MasterQuestAnsWrapper> createAssociationQuestRec(String masterId, String associationId, List<String> listofAns) {
        System.debug('--masterId--'+masterId+'----associationId---'+associationId);
        System.debug('--listofAns--'+listofAns);
        System.debug('--listofAns--size--'+listofAns.size());
        
        List<MasterQuestAnsWrapper> masterQuestAnsWrapList = new List<MasterQuestAnsWrapper>();
        MasterQuestAnsWrapper masterQuestAnsWrap = new MasterQuestAnsWrapper();
        List<Association_Question__c> newAssociationQuestList = new List<Association_Question__c>();
        
        List<String> dataTypeList = new List<String>();
        try{
            
            Master_Question__c masterQuestObj = [ SELECT Id,
                                                 Master_Question__c,
                                                 Category__c,
                                                 Field_Data_Type__c,
                                                 LastModifiedBy.LastModifiedDate
                                                 FROM Master_Question__c
                                                 WHERE Id = : masterId
                                                 LIMIT 1];
            System.debug('masterQuestObj=='+masterQuestObj.Id);
            
            List<Master_Question_Answer_Values__c> masterQuestAnsList = new List<Master_Question_Answer_Values__c>();
            if(!listofAns.isEmpty() || listofAns.size() > 0) {
                masterQuestAnsList =  [SELECT Id,
                                       Name
                                       FROM Master_Question_Answer_Values__c
                                       WHERE Master_Question__c =: masterQuestObj.Id 
                                       AND Id IN : listofAns];
                System.debug('masterQuestAnsList===if part=='+masterQuestAnsList);
            }else {
                masterQuestAnsList = [SELECT Id,
                                      Name
                                      FROM Master_Question_Answer_Values__c
                                      WHERE Master_Question__c =: masterQuestObj.Id];
                
                System.debug('masterQuestAnsList===else part=='+masterQuestAnsList);
                
            }
            
            System.debug('masterQuestAnsList==outer==='+masterQuestAnsList);
            
            Association_Question__c assQuestObj = new Association_Question__c();
            assQuestObj.Displayed_Question__c = masterQuestObj.Master_Question__c;
            assQuestObj.Association__c = associationId;
            assQuestObj.Required__c = true;
            assQuestObj.View_in_Member_Directory__c = true;
            assQuestObj.Master_Question__c = masterQuestObj.Id;
            
            //insert assQuestObj;
            System.debug('--114--'+assQuestObj);
            newAssociationQuestList.add(assQuestObj);
            insert newAssociationQuestList;
            System.debug('--118--'+newAssociationQuestList);
            masterQuestAnsWrap.associationQuestList = newAssociationQuestList;
            
            List<Association_Question_Answer_Value__c> associationQAnsList = new List<Association_Question_Answer_Value__c>();
            
            for(Master_Question_Answer_Values__c masterQuestAns : masterQuestAnsList) {
                System.debug('--masterQuestAns==='+masterQuestAns);
                Association_Question_Answer_Value__c associationQuestAns = new Association_Question_Answer_Value__c();
                associationQuestAns.Name = masterQuestAns.Name;
                associationQuestAns.Master_Question_Answer_Value__c = masterQuestAns.Id;
                associationQuestAns.Association_Question__c = newAssociationQuestList[0].Id;
                associationQAnsList.add(associationQuestAns);
            }
            
            insert associationQAnsList;
            System.debug('--associationQAnsList==='+associationQAnsList);
            /*if(associationQAnsList.size() >0){
                masterQuestAnsWrap.isDelete = true;
            }else {
                masterQuestAnsWrap.isDelete = false;
            }*/
            Map<Id,List<Association_Question_Answer_Value__c>> mapOfAssociationQuestAns = new Map<Id,List<Association_Question_Answer_Value__c>>();
            for(Association_Question_Answer_Value__c associationQuest : associationQAnsList) {
                if(!mapOfAssociationQuestAns.containsKey(associationQuest.Association_Question__c)) {
                    mapOfAssociationQuestAns.put(associationQuest.Association_Question__c,new List<Association_Question_Answer_Value__c>{associationQuest});    
                } else {
                    
                    List<Association_Question_Answer_Value__c> listPreviousAssociationQueAns = new List<Association_Question_Answer_Value__c>();  
                    listPreviousAssociationQueAns.addAll(mapOfAssociationQuestAns.get(associationQuest.Association_Question__c));
                    listPreviousAssociationQueAns.add(associationQuest);
                    
                    mapOfAssociationQuestAns.put(associationQuest.Association_Question__c,listPreviousAssociationQueAns);
                } // end of if-else block
            }
            System.debug('mapOfAssociationQuestAns========='+mapOfAssociationQuestAns);
            masterQuestAnsWrap.mapOfAssociationQuestAnsVal.putAll(mapOfAssociationQuestAns);
            masterQuestAnsWrap.newChildAccordion = false;
            masterQuestAnsWrap.checkEye = false;
            masterQuestAnsWrap.isAssAnsValue = false;
            masterQuestAnsWrap.isDelete = true;
            masterQuestAnsWrapList.add(masterQuestAnsWrap);
        }catch(Exception ex){
            System.debug('ex========='+ex.getMessage());
            System.debug('ex========='+ex.getLineNumber());
        }
        return masterQuestAnsWrapList;
    }
    
    @AuraEnabled
    public static deleteAssociationWrapper deletedAssociationRec(Id associationQuestId) {
        System.debug('===associationQuestId=='+associationQuestId);
        deleteAssociationWrapper wrapperRecord = new deleteAssociationWrapper();
        
        try{
            
            Association_Question__c assQuest = [SELECT Id,
                                                Master_Question__c
                                                FROM Association_Question__c
                                                WHERE Id =: associationQuestId];
            
            System.debug('assQuest=='+assQuest);
            
            Master_Question__c masterQuestObj = [ SELECT Id,
                                                 Master_Question__c,
                                                 Category__c,
                                                 Field_Data_Type__c,
                                                 LastModifiedBy.LastModifiedDate
                                                 FROM Master_Question__c
                                                 WHERE Id = : assQuest.Master_Question__c];
            System.debug('masterQuestObj=='+masterQuestObj.Id);
            
            Delete assQuest;
            wrapperRecord.succMsg = 'Association Record Deleted Successfully!'; 
            wrapperRecord.masterQRecord = masterQuestObj;
        }catch(Exception ex) {
            System.debug('===error=='+ex.getMessage());
            //msg = ex.getMessage();
        }
        return wrapperRecord;
    }
    
    @AuraEnabled
    public static List<QuestionsWrapper> deletedAssociationRec1(Id associationQuestId) {
        System.debug('===associationQuestId=='+associationQuestId);        
        Association_Question__c assQuest = [SELECT Id,
                                            Master_Question__c
                                            FROM Association_Question__c
                                            WHERE Id =: associationQuestId];
        
        List<QuestionsWrapper> questWrapperList = new List<QuestionsWrapper>();
        
        for(Master_Question__c masterQObj : [SELECT Id,
                                             Master_Question__c,
                                             Category__c,
                                             Field_Data_Type__c,
                                             LastModifiedBy.LastModifiedDate,
                                             (SELECT Id,
                                              Name,Value__c
                                              FROM Question_Answer_Values__r)
                                             FROM Master_Question__c 
                                             WHERE Status__c = 'Published' AND Id=:assQuest.Master_Question__c]) {
                                                 QuestionsWrapper questWrapper = new QuestionsWrapper();
                                                 //System.debug('==FOR===masterQObj=====' + masterQObj);
                                                 questWrapper.checkAccordion = false;
                                                 questWrapper.checkChildAccordion = false;
                                                 questWrapper.checkAdd = false;
                                                 questWrapper.success = true;
                                                 questWrapper.message = 'Association Record Deleted Successfully!';
                                                 List<Master_Question__c> tempMasterQList = new List<Master_Question__c>();
                                                 tempMasterQList =  questWrapper.masterQuestsList;
                                                 
                                                     tempMasterQList.add(masterQObj);
                                                     system.debug('tempMasterQList==='+tempMasterQList);
                                                     questWrapper.masterQuestsList = tempMasterQList;
                                                     
                                                     //}
                                                     //}
                                                     
                                                     system.debug('wrapflll==<<'+questWrapper.masterQuestsList);
                                                     //questWrapperList.add(questWrapper);
                                                     
                                                     // 08.08.2019
                                                     Map<Id,List<Master_Question_Answer_Values__c>> mapMasterQueIdVSMasterQueAnsList = new Map<Id,List<Master_Question_Answer_Values__c>>(); 
                                                     System.debug('=====masterQObj.Question_Answer_Values__r.size()=====' + masterQObj.Question_Answer_Values__r.size()); 
                                                     for(Master_Question_Answer_Values__c masterQuesAnsObj : masterQObj.Question_Answer_Values__r) {
                                                         //System.debug('=====masterQuesAnsObj=====' + masterQuesAnsObj); 
                                                         
                                                         if(!mapMasterQueIdVSMasterQueAnsList.containsKey(masterQObj.Id)) {
                                                             mapMasterQueIdVSMasterQueAnsList.put(masterQObj.Id,new List<Master_Question_Answer_Values__c>{masterQuesAnsObj});    
                                                         } else {
                                                             List<Master_Question_Answer_Values__c> listPreviousMasterQueAns = new List<Master_Question_Answer_Values__c>();  
                                                             listPreviousMasterQueAns.addAll(mapMasterQueIdVSMasterQueAnsList.get(masterQObj.Id));
                                                             listPreviousMasterQueAns.add(masterQuesAnsObj);
                                                             mapMasterQueIdVSMasterQueAnsList.put(masterQObj.Id,listPreviousMasterQueAns);
                                                         } // end of if-else block
                                                     } // End of inner for
                                                     //System.debug('mapMasterQueIdVSMasterQueAnsList========='+mapMasterQueIdVSMasterQueAnsList);
                                                     questWrapper.mapOfMasterQuestAns.putAll(mapMasterQueIdVSMasterQueAnsList);
                                                     questWrapperList.add(questWrapper);
                                                 } // End of outer for
        
        System.debug('==questWrapperList=='+questWrapperList);
        Delete assQuest;
        return questWrapperList;
        
    }
    
    @AuraEnabled
    public static List<MasterQuestAnsWrapper> getAssociationQuestionRec1(String associationId) {
        System.debug('=296--=associationId=='+associationId);
        List<MasterQuestAnsWrapper> relatedQuestWrapperList = new List<MasterQuestAnsWrapper>();
        Set<Id> setOfAssociationMember = new Set<Id>();
        
        try{ 
            for(Association_Question__c associationQObj : [SELECT Id,Association__c,
                                                           Displayed_Question__c,
                                                           Required__c,
                                                           View_in_Member_Directory__c,
                                                           (SELECT Id,
                                                            Value__c
                                                            FROM Association_Question_Answer_Values__r),
                                                           (SELECT Id, 
                                                            Association_Question__c
                                                            FROM Member_Answers__r)
                                                           FROM Association_Question__c
                                                           WHERE Association__c =: associationId ]) {
                                                               //setOfAssociationMember.add(masterQObj.Id);
                                                               MasterQuestAnsWrapper questWrapper = new MasterQuestAnsWrapper();
                                                               //System.debug('==FOR===masterQObj=====' + masterQObj);
                                                               if(associationQObj.Association__c != null) {
                                                                   System.debug('--association not null for Association Question');
                                                                   List<Association_Question__c> tempAssociationQList = new List<Association_Question__c>();
                                                                   tempAssociationQList =  questWrapper.associationQuestList;
                                                                   tempAssociationQList.add(associationQObj);
                                                                   system.debug('tempMasterQList==='+tempAssociationQList);
                                                                   questWrapper.associationQuestList = tempAssociationQList;
                                                                   
                                                                   if(!associationQObj.Member_Answers__r.isEmpty()){
                                                                       System.debug('--Memeber Answers--');
                                                                       questWrapper.checkEye = true;
                                                                   }else {
                                                                       questWrapper.checkEye = false;
                                                                       questWrapper.isDelete = true;
                                                                   }
                                                                   //system.debug('wrapflll==<<'+questWrapper.associationQuestList);
                                                                   // 08.08.2019
                                                                   Map<Id,List<Association_Question_Answer_Value__c>> mapMasterQueIdVSMasterQueAnsList = new Map<Id,List<Association_Question_Answer_Value__c>>(); 
                                                                   //System.debug('=====masterQObj.Question_Answer_Values__r.size()=====' + masterQObj.Question_Answer_Values__r.size()); 
                                                                   /*if(associationQObj.Association_Question_Answer_Values__r.isEmpty()) {
                                                                       questWrapper.isDelete = false;
                                                                   }else {
                                                                       questWrapper.isDelete = true;
                                                                   }*/
                                                                   for(Association_Question_Answer_Value__c assQuesAnsObj : associationQObj.Association_Question_Answer_Values__r) {
                                                                       //System.debug('=====assQuesAnsObj=====' + assQuesAnsObj);
                                                                       if(!mapMasterQueIdVSMasterQueAnsList.containsKey(associationQObj.Id)) {
                                                                           mapMasterQueIdVSMasterQueAnsList.put(associationQObj.Id,new List<Association_Question_Answer_Value__c>{assQuesAnsObj});    
                                                                       } else {
                                                                           List<Association_Question_Answer_Value__c> listPreviousMasterQueAns = new List<Association_Question_Answer_Value__c>();  
                                                                           listPreviousMasterQueAns.addAll(mapMasterQueIdVSMasterQueAnsList.get(associationQObj.Id));
                                                                           listPreviousMasterQueAns.add(assQuesAnsObj);
                                                                           mapMasterQueIdVSMasterQueAnsList.put(associationQObj.Id,listPreviousMasterQueAns);
                                                                       } // end of if-else block
                                                                   } // End of inner for
                                                                   System.debug('mapMasterQueIdVSMasterQueAnsList========='+mapMasterQueIdVSMasterQueAnsList);
                                                                   questWrapper.mapOfAssociationQuestAnsVal.putAll(mapMasterQueIdVSMasterQueAnsList);
                                                                   relatedQuestWrapperList.add(questWrapper);
                                                               } // End of outer for
                                                               System.debug('questWrapper==========='+questWrapper);  
                                                           }
            
             
        }catch(Exception e){
            System.debug('e==========='+e.getMessage()+'--line--'+e.getLineNumber());
            ///questWrapper.message = e.getMessage();
            //questWrapper.success = false;
        }
        System.debug('==questWrapperList=='+relatedQuestWrapperList);
        return relatedQuestWrapperList;
        
    }
    
    // wrapper class for Master Questions
    public class QuestionsWrapper{
        @AuraEnabled
        public String message;
        @AuraEnabled
        public List<Master_Question__c> masterQuestsList = new List<Master_Question__c>();
        @AuraEnabled
        public boolean checkAdd;
        //@AuraEnabled
        //public boolean checkChildAdd;
        @AuraEnabled
        public boolean checkChildAccordion;
        //@AuraEnabled
        //public boolean checkEye;
        @AuraEnabled
        public boolean checkAccordion;
        @AuraEnabled
        public Map<Id,List<Master_Question_Answer_Values__c>> mapOfMasterQuestAns = new Map<Id,List<Master_Question_Answer_Values__c>>();
        @AuraEnabled
        public Boolean success;
        
        
    }
    
    
    // wrapper class 
    public class MasterQuestAnsWrapper{
        
        @AuraEnabled
        public List<String> dataTypeValue;
        @AuraEnabled
        public List<Master_Question_Answer_Values__c> masterQuestAnsList;
        @AuraEnabled
        public List<Association_Question__c> associationQuestList = new List<Association_Question__c>();
        @AuraEnabled
        public Boolean newChildAccordion = false;
        @AuraEnabled
        public Boolean checkEye = false;
        @AuraEnabled
        public Boolean isDelete = false;
        @AuraEnabled
        public Boolean isAssAnsValue = false;
        @AuraEnabled
        public Map<Id,List<Association_Question_Answer_Value__c>> mapOfAssociationQuestAnsVal = new Map<Id,List<Association_Question_Answer_Value__c>>();
        
    }
    
    public class PickListWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        
        public PickListWrapper(String label, String val) {
            this.label = label;
            this.value = val;
        }
        
    }
    
    public class deleteAssociationWrapper {
        @AuraEnabled
        public String succMsg;
        @AuraEnabled
        public Master_Question__c masterQRecord = new Master_Question__c();
        
    }
    
    
}