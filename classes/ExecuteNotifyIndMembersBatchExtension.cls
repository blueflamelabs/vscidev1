/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                  ModofiedDate  ModifiedBy              Description
*     1.0        120819         VennScience_BFL_Amruta                                           This is the extension class for ExecuteNotifyIndividualMembersBatch visualforce page.
*     
*     1.1                                                  190919        VennScience_BFL_Amruta  Pass parent Association Id of Account to batch class
**********************************************************************************************************************************************************/
public class ExecuteNotifyIndMembersBatchExtension {

    Account getAccRecord;
    Account fetchAccRecord;
    public ExecuteNotifyIndMembersBatchExtension(ApexPages.StandardController controller) {
        getAccRecord = new Account();
        fetchAccRecord = new Account();
        getAccRecord = (Account)controller.getRecord();
    }
    public PageReference executeBatch() {
        PageReference pg;
        if(getAccRecord.Id != null) {
            // T-00686 - 190919 - VennScience_BFL_Amruta - Fetch parent Association for current Account and pass the same to batch class
            fetchAccRecord  = [SELECT Id,
                                      Association__c
                                 FROM Account
                                WHERE Id = :getAccRecord.Id];
            Database.executeBatch(new NotifyIndividualMembersBatch(getAccRecord.Id,fetchAccRecord.Association__c));
            pg = new PageReference('/'+getAccRecord.Id);
            pg.setRedirect(true);
            //return pg;  
        }
        return pg;
    }
}