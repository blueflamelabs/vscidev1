/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         150619      VennScience_BFL_Monali       This class is built for test utility.
**********************************************************************************************************************************************************/ 
@isTest
public class TestDataFactory {
    public static Account getAcct(){
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Association Member Account').getRecordTypeId();
        
        Account ac = new Account();
        ac.RecordTypeId = recTypeId;
        ac.Name = 'Test Parent' ;
        //ac.Expiration_Date__c = System.today() - 2 ;
        return ac;
    }
    public static Account getchildAcct(){
        
        // 230819 - VennScience_BFL_Amruta - Insert Custom Setting
        Individual_Members_Email_Alerts_Setting__c objCustomSetting = new Individual_Members_Email_Alerts_Setting__c();
        objCustomSetting = getCustomSettingRecord();
        // System.debug('Custom Setting in test class========'+objCustomSetting.Account_Record_Type_Name__c);
        //Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member Account').getRecordTypeId();
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                                   .get(objCustomSetting.Account_Record_Type_Name__c)
                                   .getRecordTypeId();
        Account ac = new Account();
        ac.RecordTypeId = recTypeId;
        // ac.Association_Account__c = parentAcountId;
        ac.firstName = 'Test Individual' ;
        ac.LastName = 'Member';
        ac.Expiration_Manual_Extension__c = System.today() - 3;
        ac.Next_Renewal_Date__c = System.today() + 7;
        ac.PersonEmail = 'test@gmail.com';
        ac.Member_Category__pc = 'Primary Contact';
        return ac;
    }
    /**
    * Method Name : getCorporateRepresentativeAccount
    * Parameters  : 
    * Description : Used to create Account records of Corporate Representatives record type
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 190919
    **/
    public static Account getCorporateRepresentativeAccount(){
        
        // 230819 - VennScience_BFL_Amruta - Insert Custom Setting
        Individual_Members_Email_Alerts_Setting__c objSettingToBeDeleted = new Individual_Members_Email_Alerts_Setting__c();
        objSettingToBeDeleted = Individual_Members_Email_Alerts_Setting__c.getOrgDefaults();
        if(objSettingToBeDeleted.Id != null) {
            delete objSettingToBeDeleted;
        }
        Individual_Members_Email_Alerts_Setting__c objCustomSetting = new Individual_Members_Email_Alerts_Setting__c();
        objCustomSetting = getIndMemCustomSettingRecord();
        // System.debug('Custom Setting in test class========'+objCustomSetting.Account_Record_Type_Name__c);
        //Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member Account').getRecordTypeId();
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                                   .get(objCustomSetting.Account_Record_Type_Name__c)
                                   .getRecordTypeId();
        Account ac = new Account();
        ac.RecordTypeId = recTypeId;
        // ac.Association_Account__c = parentAcountId;
        ac.firstName = 'Test Corporate' ;
        ac.LastName = 'Representative';
        ac.Expiration_Manual_Extension__c = System.today() - 3;
        ac.Next_Renewal_Date__c = System.today() + 7;
        ac.PersonEmail = 'test@gmail.com';
        ac.Member_Category__pc = 'Primary Contact';
        return ac;
    }
    /**
    * Method Name : createAssociationRecords
    * Parameters  : 
    * Description : Used to create Association records
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
     public static Association__c createAssociationRecords() {
         List<EmailTemplate> emailTemplateList = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'First_Renewal_Alert'
                                                 OR DeveloperName = 'Second_Renewal_Alert'
                                                 OR DeveloperName = 'Third_Renewal_Alert'
                                                 OR DeveloperName = 'First_Expiration_Alert'
                                                 OR DeveloperName = 'Second_Expiration_Alert'
                                                 OR DeveloperName = 'Final_Termination_Notice'];
         
         Map<String, Id> emailTemplateAPINameVsEmailTemplateIdMap = new Map<String, Id>();
         
         for(EmailTemplate emailTemplateObj : emailTemplateList) {
             
             emailTemplateAPINameVsEmailTemplateIdMap.put(emailTemplateObj.DeveloperName, emailTemplateObj.Id);
         }
         Association__c objAssociation = new Association__c();
         objAssociation.Name = 'Test Association';
         /*
         if(!emailTemplateAPINameVsEmailTemplateIdMap.isEmpty()) {
         	 objAssociation.First_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('First_Renewal_Alert');
             objAssociation.Second_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Second_Renewal_Alert');
             objAssociation.First_Expiration_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('First_Expiration_Alert');
             objAssociation.Third_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Third_Renewal_Alert');
             objAssociation.Second_Expiration_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Second_Expiration_Alert');
             objAssociation.Final_Termination_Notice_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Final_Termination_Notice');    
        	   
		 }*/
         // 12.09.2019 - T-00637 - VennScience_BFL_Amruta - Populate Email Template field with Template's API name
         objAssociation.First_Renewal_Alert_Email_Template__c = 'First_Renewal_Alert';
         objAssociation.Second_Renewal_Alert_Email_Template__c = 'Second_Renewal_Alert';
         objAssociation.First_Expiration_Alert_Email_Template__c = 'First_Expiration_Alert';
         objAssociation.Third_Renewal_Alert_Email_Template__c = 'Third_Renewal_Alert';
         objAssociation.Second_Expiration_Alert_Email_Template__c = 'Second_Expiration_Alert';
         objAssociation.Final_Termination_Notice_Email_Template__c = 'Final_Termination_Notice'; 
         return objAssociation;
     } 
    @isTest
    public static Individual_Members_Email_Alerts_Setting__c getCustomSettingRecord() {
        Individual_Members_Email_Alerts_Setting__c settings = Individual_Members_Email_Alerts_Setting__c.getOrgDefaults();
        settings.SetupOwnerId = UserInfo.getOrganizationId();
        settings.Account_Record_Type_Name__c = 'IndividualAccount';
        settings.Account_Member_Category__c = 'Primary Contact';
        settings.Default_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert settings;
        return settings;
    }
    @isTest
    public static Individual_Members_Email_Alerts_Setting__c getIndMemCustomSettingRecord() {
        Individual_Members_Email_Alerts_Setting__c settings = Individual_Members_Email_Alerts_Setting__c.getOrgDefaults();
        settings.SetupOwnerId = UserInfo.getOrganizationId();
        settings.Account_Record_Type_Name__c = 'Corporate_Representative';
        settings.Account_Member_Category__c = 'Primary Contact';
        settings.Default_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert settings;
        return settings;
    }
    /**
    * Method Name : getTerminationCustomSettingRecord
    * Parameters  : 
    * Description : Used to create custom setting records for 'Termination Date Update Setting' custom setting
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static Termination_Date_Update_Setting__c getTerminationCustomSettingRecord() {
        Termination_Date_Update_Setting__c settings = Termination_Date_Update_Setting__c.getOrgDefaults();
        settings.SetupOwnerId = UserInfo.getOrganizationId();
        settings.Corporate_Member_RecordType_Name__c = 'Association_Account';
        settings.Individual_Member_s_RecordType_Name__c = 'IndividualAccount';
        insert settings;
        return settings;
    }
    
    /**
    * Method Name : createBulkAssociationRecords
    * Parameters  : 
    * Description : Used to create bulk Association records
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    
    public static List<Association__c> createBulkAssociationRecords(Integer recordCount) {
         List<Association__c> listAssociation = new List<Association__c>();
         List<EmailTemplate> emailTemplateList = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'First_Renewal_Alert'
                                                 OR DeveloperName = 'Second_Renewal_Alert'
                                                 OR DeveloperName = 'Third_Renewal_Alert'
                                                 OR DeveloperName = 'First_Expiration_Alert'
                                                 OR DeveloperName = 'Second_Expiration_Alert'
                                                 OR DeveloperName = 'Final_Termination_Notice'];
         
         Map<String, Id> emailTemplateAPINameVsEmailTemplateIdMap = new Map<String, Id>();
         
         for(EmailTemplate emailTemplateObj : emailTemplateList) {
             
             emailTemplateAPINameVsEmailTemplateIdMap.put(emailTemplateObj.DeveloperName, emailTemplateObj.Id);
         }
         for(Integer i=0; i < recordCount; i++) {
            Association__c objAssociation = new Association__c();
            objAssociation.Name = 'Test Association'+i;
            objAssociation.First_Renewal_Alert_Send_Days__c = 5;
        	objAssociation.Second_Renewal_Alert_Send_Days__c = 4;
        	objAssociation.First_Expiration_Alert_Send_Days__c= 3;
        	objAssociation.Email_Sender_Address__c = 'vforceadmin@virtualinc.com';
             /*
            if(!emailTemplateAPINameVsEmailTemplateIdMap.isEmpty()) {
                objAssociation.First_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('First_Renewal_Alert');
                objAssociation.Second_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Second_Renewal_Alert');
                objAssociation.First_Expiration_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('First_Expiration_Alert');
                objAssociation.Third_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Third_Renewal_Alert');
                objAssociation.Second_Expiration_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Second_Expiration_Alert');
                objAssociation.Final_Termination_Notice_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Final_Termination_Notice');    
            } // End of if
			*/
            // 12.09.2019 - T-00637 - VennScience_BFL_Amruta - Populate Email Template field with Template's API name
             objAssociation.First_Renewal_Alert_Email_Template__c = 'First_Renewal_Alert';
             objAssociation.Second_Renewal_Alert_Email_Template__c = 'Second_Renewal_Alert';
             objAssociation.First_Expiration_Alert_Email_Template__c = 'First_Expiration_Alert';
             objAssociation.Third_Renewal_Alert_Email_Template__c = 'Third_Renewal_Alert';
             objAssociation.Second_Expiration_Alert_Email_Template__c = 'Second_Expiration_Alert';
             objAssociation.Final_Termination_Notice_Email_Template__c = 'Final_Termination_Notice';    
            listAssociation.add(objAssociation);
         } // End of for
         return listAssociation;
     } 
     /**
     * Method Name : createBulkAccountRecords
     * Parameters  : 
     * Description : Used to create bulk Association records
     * Created By  : VennScience_BFL_Amruta 
     * Created On  : 040919
     **/
     
     public static List<Account> createBulkAccountRecords(List<Association__c> listParentAssociations){
        
        List<Account> listAccount = new List<Account>();
        Individual_Members_Email_Alerts_Setting__c objCustomSetting = new Individual_Members_Email_Alerts_Setting__c();
        objCustomSetting = getCustomSettingRecord();
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                                   .get(objCustomSetting.Account_Record_Type_Name__c)
                                   .getRecordTypeId();
        for(Integer i=0; i < listParentAssociations.size(); i++) {
            Account ac = new Account();
            ac.RecordTypeId = recTypeId;
            ac.firstName = 'Test Individual' ;
            ac.LastName = 'Member';
            ac.Expiration_Manual_Extension__c = System.today() - 3;
            ac.Next_Renewal_Date__c = System.today() + 7;
            ac.PersonEmail = 'test@gmail.com';
            ac.Member_Category__pc = 'Primary Contact';
            ac.Association__c = listParentAssociations[i].Id;
            listAccount.add(ac);
        }
        return listAccount;
    }
}