@isTest
public class LeadConvertController_Test{
    public static testmethod void leadConvertControllermethod(){
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
            
            Working_Group__c workgroup = new Working_Group__c();
            workgroup.Association__c = ass.id;
            insert workgroup;
            
            Association_Question__c assque = new Association_Question__c();
            assque.Association__c = ass.id;
            insert assque;
            
            Member_Type__c member =new Member_Type__c();
            member.Association__c =ass.id;
            member.Name ='Test';
            insert member;
                   
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.Name = 'Test Parent' ;
           acc.Company_Association_Membership__c =ass.id ;
           acc.Association__c = ass.id;
           insert acc;
           
           Contact objCon =new Contact();
           objCon.LastName ='Test';
           objCon.AccountId =acc.id;
           insert objCon;
           
           Lead objLead =new Lead();
           objLead.LastName ='Test';
           objLead.Association__c =ass.id;
           objLead.Member_Type__c =member.id;
           insert objLead;
           
           
           Additional_Contacts__c objAdditionalcon =new Additional_Contacts__c();
           objAdditionalcon.Lead__c =objLead.id;
           objAdditionalcon.Last_Name__c ='Test';
           insert objAdditionalcon;
           
           Working_Group_Interest__c objWorkinginsterest =new Working_Group_Interest__c();
           objWorkinginsterest.Working_Group__c =workgroup.id;
           insert objWorkinginsterest;
           
           Member_Answer__c objMemberans =new Member_Answer__c();
           objMemberans.Lead__c =objLead.id;
           objMemberans.Member__c =objCon.id;
           insert objMemberans;
           
            ApexPages.currentPage().getParameters().put('Id',ObjLead.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(ObjLead);
            LeadConvertController leadconcontroller = new LeadConvertController(sc);
            leadconcontroller.LeadConvert();
            
      }
       public static testmethod void leadConvertControllermethod1(){
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
            
            Working_Group__c workgroup = new Working_Group__c();
            workgroup.Association__c = ass.id;
            insert workgroup;
            
            Association_Question__c assque = new Association_Question__c();
            assque.Association__c = ass.id;
            insert assque;
            
            Member_Type__c member =new Member_Type__c();
            member.Association__c =ass.id;
            member.Name ='Test';
            insert member;
                   
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.Name = 'Test Parent' ;
           acc.Company_Association_Membership__c =ass.id ;
           insert acc;
           
           Contact objCon =new Contact();
           objCon.LastName ='Test';
           objCon.AccountId =acc.id;
           insert objCon;
           
           Lead objLead =new Lead();
           objLead.LastName ='Test';
           objLead.Association__c =ass.id;
           objLead.Member_Type__c =member.id;
           objLead.Company_Name__c ='Syncrasy';
           insert objLead;
          
           Additional_Contacts__c objAdditionalcon =new Additional_Contacts__c();
           objAdditionalcon.Lead__c =objLead.id;
           objAdditionalcon.Last_Name__c ='Test';
           insert objAdditionalcon;
           
           Working_Group_Interest__c objWorkinginsterest =new Working_Group_Interest__c();
           objWorkinginsterest.Working_Group__c =workgroup.id;
           insert objWorkinginsterest;
           
           Member_Answer__c objMemberans =new Member_Answer__c();
           objMemberans.Lead__c =objLead.id;
           objMemberans.Member__c =objCon.id;
           insert objMemberans;
           
            ApexPages.currentPage().getParameters().put('Id',ObjLead.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(ObjLead);
            LeadConvertController leadconcontroller = new LeadConvertController(sc);
            leadconcontroller.LeadConvert();
            
            
            
      }
      
     public static testmethod void leadConvertControllermethod2(){
     
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
            
            Working_Group__c workgroup = new Working_Group__c();
            workgroup.Association__c = ass.id;
            insert workgroup;
            
            Association_Question__c assque = new Association_Question__c();
            assque.Association__c = ass.id;
            insert assque;
            
            Member_Type__c member =new Member_Type__c();
            member.Association__c =ass.id;
            member.Name ='Test';
            insert member;
                   
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Account').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.Name = 'Test Parent' ;
           acc.Company_Association_Membership__c =ass.id ;
           acc.Association__c = ass.id;
           insert acc;
           
           Account acco = new Account();
           acco.Name = 'Test Parent' ;
           insert acco;
           
           Lead objLead =new Lead();
           objLead.LastName ='Test';
           objLead.Association__c =ass.id;
           objLead.Member_Type__c =member.id;
           objLead.Company ='Syncrasy';
           objLead.Corporate_Account__c=acc.id;
           insert objLead;
            
           Additional_Contacts__c objAdditionalcon =new Additional_Contacts__c();
           objAdditionalcon.Lead__c =objLead.id;
           objAdditionalcon.Last_Name__c ='Test';
           insert objAdditionalcon;
            
           
           Contact objCon =new Contact();
           objCon.LastName ='Test';
           objCon.AccountId =acc.id;
           objCon.Additional_ContactID__c=objAdditionalcon.id;
           insert objCon;
          
           Working_Group_Interest__c objWorkinginsterest =new Working_Group_Interest__c();
           objWorkinginsterest.Working_Group__c =workgroup.id;
           insert objWorkinginsterest;
           
           Member_Answer__c objMemberans =new Member_Answer__c();
           objMemberans.Lead__c =objLead.id;
           objMemberans.Member__c =objCon.id;
           insert objMemberans;
           
            ApexPages.currentPage().getParameters().put('Id',ObjLead.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(ObjLead);
            LeadConvertController leadconcontroller = new LeadConvertController(sc);
            leadconcontroller.LeadConvert();
      
      }
      public static testmethod void leadConvertControllermethod3(){
     
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
            
            Working_Group__c workgroup = new Working_Group__c();
            workgroup.Association__c = ass.id;
            insert workgroup;
            
            Association_Question__c assque = new Association_Question__c();
            assque.Association__c = ass.id;
            insert assque;
            
            Member_Type__c member =new Member_Type__c();
            member.Association__c =ass.id;
            member.Name ='Test';
            insert member;
            
          Id AccRecordTypeIdCor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Account').getRecordTypeId();
           Account accCor = new Account();
           accCor.RecordTypeId = AccRecordTypeIdCor;
           accCor.Name = 'Test Parent' ;
           accCor.Company_Association_Membership__c =ass.id ;
           insert accCor;   
            
           Lead objLead =new Lead();
           objLead.LastName ='Test';
           objLead.FirstName = 'Test Parent' ;
           objLead.Association__c =ass.id;
           objLead.Member_Type__c =member.id;
           objLead.Join_Date__c =date.today();
           objLead.Corporate_Account__c=accCor.id;
           insert objLead; 
            
            
         Id AccRecordTypeIdPer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
           Account accPer = new Account();
           accPer.RecordTypeId = AccRecordTypeIdPer;
           accPer.FirstName = 'Test Parent' ;
           accPer.LastName = 'Test' ;
           insert accPer;    
            
           
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.Person_Account__c=accPer.id;
           acc.FirstName = 'Test Parent';
           acc.LastName = 'Test' ;
           acc.Association__c = ass.id;
           insert acc;
          
            
           Additional_Contacts__c objAdditionalcon =new Additional_Contacts__c();
           objAdditionalcon.Lead__c =objLead.id;
           objAdditionalcon.Last_Name__c ='Test';
           insert objAdditionalcon;
            
           
           Contact objCon =new Contact();
           objCon.LastName ='Test';
           objCon.AccountId =accCor.id;
           objCon.Additional_ContactID__c=objAdditionalcon.id;
           insert objCon;
          
           Working_Group_Interest__c objWorkinginsterest =new Working_Group_Interest__c();
           objWorkinginsterest.Working_Group__c =workgroup.id;
           insert objWorkinginsterest;
           
           Member_Answer__c objMemberans =new Member_Answer__c();
           objMemberans.Lead__c =objLead.id;
           objMemberans.Member__c =objCon.id;
           insert objMemberans;
           
            ApexPages.currentPage().getParameters().put('Id',ObjLead.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(ObjLead);
            LeadConvertController leadconcontroller = new LeadConvertController(sc);
            leadconcontroller.LeadConvert();
      
      }
}