/*********************************************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                 ModifiedDate  ModifiedBy              Description
*      1.0       230719         VennScience_BFL_Amruta      -      			-					This Batch is built to send email notification to Individual Members
*    
*	   1.1         -	        VennScience_BFL_Amruta    010819        VennScience_BFL_Amruta  Execute the Renewal/Expiration email alert functionality for Third Renewal
*                                                                                               alert, second expiration alert and final expiration email alert as well.
*
*      1.2         -            VennScience_BFL_Amruta    130819		VennScience_BFL_Amruta	Execute the Renewal/Expiration email alert functionality single Account record                                                      
*                                                           									for which the 'Test Renewal Email' button is clicked from Account's detail page. 
*
* 	   2.0         -            VennScience_BFL_Amruta    180919		VennScience_BFL_Amruta	Support Individual member and Corporate Representative account record type 
*													  											for Renewal/Expiration email alert functionality
**********************************************************************************************************************************************************************************/   
global class NotifyIndividualMembersBatch implements Database.Batchable<Sobject> {
    // Variable Declarations
    public Id individualMemId;
    public Id parentAssociationId;
    
    // Parameterised Constructor(Called when Test Renewal Email button is clicked from Account detail page)
    public NotifyIndividualMembersBatch(Id recordId, Id assId) {
        individualMemId = recordId;
        parentAssociationId = assId;
    }
    // Default Constructor
    public NotifyIndividualMembersBatch() {
    
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        // Variable Declarations
        String query = '';
		
        /* T-00686 - V2.0 - 190919 - VennScience_BFL_Amruta - Modified below code to support Individual Memeber and Corporate Representative
           account record types for this functionality */
        // T-00510 - V1.2 - 130819 - VennScience_BFL_Amruta - Added condition to run for specific record when batch is executed from
        // detail page button on Individual Member
        if(individualMemId != null) {
            query = 'SELECT Id,'
                    +'Name,'
                    +'First_Renewal_Alert_Email_Template__c,'
                    +'First_Renewal_Alert_Send_Days__c,'
                    +'Second_Renewal_Alert_Email_Template__c,'
                    +'Second_Renewal_Alert_Send_Days__c,'
                    +'Third_Renewal_Alert_Email_Template__c,'
                    +'Third_Renewal_Alert_Send_Days__c,'
                    +'Second_Expiration_Alert_Email_Template__c,'
                    +'Second_Expiration_Alert_Send_Days__c,'
                    +'Final_Termination_Notice_Email_Template__c,'
                    +'Final_Termination_Notice_Send_Days__c,'
                    +'First_Expiration_Alert_Email_Template__c,'
                    +'First_Expiration_Alert_Send_Days__c,'
                    +'Email_Sender_Address__c'
                    +' FROM Association__c'
                    +' WHERE Id = :parentAssociationId';
        } else {
            // T-00510 - V1.1 - 010819 - VennScience_BFL_Amruta - Added fields in Association query
            query = 'SELECT Id,'
                    +'Name,'
                    +'First_Renewal_Alert_Email_Template__c,'
                    +'First_Renewal_Alert_Send_Days__c,'
                    +'Second_Renewal_Alert_Email_Template__c,'
                    +'Second_Renewal_Alert_Send_Days__c,'
                    +'Third_Renewal_Alert_Email_Template__c,'
                    +'Third_Renewal_Alert_Send_Days__c,'
                    +'Second_Expiration_Alert_Email_Template__c,'
                    +'Second_Expiration_Alert_Send_Days__c,'
                    +'Final_Termination_Notice_Email_Template__c,'
                    +'Final_Termination_Notice_Send_Days__c,'
                    +'First_Expiration_Alert_Email_Template__c,'
                    +'First_Expiration_Alert_Send_Days__c,'
                    +'Email_Sender_Address__c'
                    +' FROM Association__c';
        }
        //System.debug('query========'+query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Association__c> listAssociationRecords) { 
        
        // Variable Declarations
        List<Account> listChildAccount = new List<Account>();
        List<String> listAccRecordTypeNameTemp = new List<String>();
        List<String> listAccRecordTypeName = new List<String>();
        Set<Id> setAssId = new Set<Id>();
        Map<Id,Association__c> mapAssIdVSAssRecord = new Map<Id,Association__c>();
        Map<Id,List<Account>> mapAssIdVSlistChildAccount = new Map<Id,List<Account>>();
        Individual_Members_Email_Alerts_Setting__c objCustomSetting = new Individual_Members_Email_Alerts_Setting__c();
        String accRecordTypeName = '';
        String accMemberCategory = '';
        
        // Fetch custom setting data
        objCustomSetting = NotifyIndividualMembersBatchHandler.getCustomSettingValues();
        // Check if there custom seting org default record is not created
        if(objCustomSetting == null || String.isBlank(objCustomSetting.Account_Record_Type_Name__c) ||
           String.isBlank(objCustomSetting.Account_Member_Category__c) || 
           String.isBlank(objCustomSetting.Default_Sender_Address__c)) {
               
           return; 
        } // End of if
        accRecordTypeName = objCustomSetting.Account_Record_Type_Name__c;
        accMemberCategory = objCustomSetting.Account_Member_Category__c;
        
        // T-00686 - V2.0 - 180919 - VennScience_BFL_Amruta - Split Account record type name seperated by commas
        if(String.isNotBlank(accRecordTypeName) && accRecordTypeName.contains(',')) {
            listAccRecordTypeNameTemp = accRecordTypeName.split(',');
        } else if(String.isNotBlank(accRecordTypeName) && !accRecordTypeName.contains(',')) {
            listAccRecordTypeNameTemp.add(accRecordTypeName);
        } // End of if-else block
        // Iterate over AccountRecordTypeName list and check if the custom setting value consist of any white spaces
        for(String strAccRecordTypeName : listAccRecordTypeNameTemp) {
            if(String.isNotBlank(strAccRecordTypeName)) {
            	listAccRecordTypeName.add(strAccRecordTypeName.trim());
            }            
        } // End of for
        // T-00686 - 190919 - VennScience_BFL_Amruta - Iterate over the scope list
        for(Association__c associationRecord : listAssociationRecords) {
            setAssId.add(associationRecord.Id);
            // T-00686 - 190919 - VennScience_BFL_Amruta - Populate map of AssociationId VS Association
            mapAssIdVSAssRecord.put(associationRecord.Id,associationRecord);
        } // End of for
        
        if(individualMemId != null) {
            // T-00686 - 190919 - VennScience_BFL_Amruta - Fetch Child Account record for which the Test Renewal button has been clicked
            listChildAccount = [SELECT Id,
                                       Name,
                                       Next_Renewal_Date__c,
                                       Expiration_Date__c,
                                       PersonEmail,
                                       Association__c,
                                       PersonContactId,
                                       Association_Account__c,
                                       Association__r.OwnerId 
                                  FROM Account
                                 WHERE RecordType.DeveloperName IN :listAccRecordTypeName 
                                   AND Member_Category__pc = :accMemberCategory
                                   AND Do_Not_Send_Automated_Emails__c = false
                                   AND Id = :individualMemId];
        } else {
        	// T-00686 - 190919 - VennScience_BFL_Amruta - Fetch Child Account records related to Association returned from scope
            listChildAccount = [SELECT Id,
                                       Name,
                                       Next_Renewal_Date__c,
                                       Expiration_Date__c,
                                       PersonEmail,
                                       Association__c,
                                       PersonContactId,
                                       Association_Account__c,
                                       Association__r.OwnerId 
                                  FROM Account
                                 WHERE RecordType.DeveloperName IN :listAccRecordTypeName 
                                   AND Member_Category__pc = :accMemberCategory
                                   AND Do_Not_Send_Automated_Emails__c = false
                                   AND Association__c IN :setAssId];    
        } // End of if-else block
        
        // T-00686 - 190919 - VennScience_BFL_Amruta - Iterate over child Account records
        for(Account accRecord : listChildAccount) {
            if(!mapAssIdVSlistChildAccount.containsKey(accRecord.Association__c)) {
                mapAssIdVSlistChildAccount.put(accRecord.Association__c,new List<Account>{accRecord});
            } else {
                List<Account> listPreviousAcc = new List<Account>();
                listPreviousAcc.addAll(mapAssIdVSlistChildAccount.get(accRecord.Association__c));
                listPreviousAcc.add(accRecord);
                // Populate AssociationId VS list of related Child Account map
                mapAssIdVSlistChildAccount.put(accRecord.Association__c,listPreviousAcc);
            } // End of else-if block     
        } // End of for
        
        // 250719 - T-00408 - VennScience_BFL_Amruta - Call handler
        NotifyIndividualMembersBatchHandler.sendEmailAlertsToMembers(mapAssIdVSAssRecord,mapAssIdVSlistChildAccount);   
    }
    global void finish(Database.BatchableContext bc) {   
    }   
}