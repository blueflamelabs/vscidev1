@IsTest
public with sharing class emailService_TEST {
    @IsTest(SeeAllData=true)
    static void sendEmail() {
        //create user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com.vscidev');
        insert u;

        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        email.plainTextBody = 'Chatter Post Test';
        email.fromAddress = 'test@test.com';
        email.subject = 'standarduser@testorg.com';
        env.toAddress='aecc-board-of-directors@r-gw9pi8qmbfvnk03cjhl3xw2nb6mhwh5mlvca9o0itpj468c9.6c-8tyxuay.cs63.apex.sandbox.salesforce.com';
        email.toAddresses = new List<String>{'aecc-board-of-directors@r-gw9pi8qmbfvnk03cjhl3xw2nb6mhwh5mlvca9o0itpj468c9.6c-8tyxuay.cs63.apex.sandbox.salesforce.com'};
        

        
        


        Test.startTest();
        emailService edr = new emailService();
        edr.handleInboundEmail(email,env);
        //Messaging.InboundEmailResult result = edr.handleInboundEmail(email, env);
        //System.assertEquals(result.success, true);
        Test.stopTest();

    }
}