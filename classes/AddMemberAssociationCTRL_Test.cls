@isTest
public class AddMemberAssociationCTRL_Test{
    public static testmethod void AddMemberAssociationCTRLmethod(){
           Association__c ass = new Association__c();
           ass.Name = 'test';
           insert ass;
            
           Member_Type__c member =new Member_Type__c();
           member.Association__c =ass.id;
           member.Name ='Test';
           insert member;
            
           Id AccRecordTypeIdPer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
           Account accPer = new Account();
           accPer.RecordTypeId = AccRecordTypeIdPer;
           accPer.FirstName = 'RajTestPer' ;
           accPer.LastName = 'Test' ;
           
          //insert Corporate Account       
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Account').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.Name = 'ASC' ;
           acc.Association__c = ass.id;
            
          //insert Corporate  Member Account
           Id AccRecordTypeIdMem = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
           Account accMem = new Account();
           accMem.RecordTypeId = AccRecordTypeIdMem;
           accMem.Name = 'Test ParentMember' ;
           accMem.Company_Association_Membership__c =ass.id ;
           
         //insert Corporate Representative Account  
           Id CorporateRepesentRecordTyprId= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
           Account accInd = new Account();
           accInd.RecordTypeId = CorporateRepesentRecordTyprId;
           accInd.FirstName = 'TestParentRep' ;
           accInd.LastName = 'Test' ;
           accInd.Person_Account__c = accPer.id;
           accInd.Association__c = ass.id;
           accInd.Member_Type__c = member.id;
           
           //insert alternatePersonAccount 
           Account alternatePersonAccount = new Account();
           alternatePersonAccount.RecordTypeId = AccRecordTypeIdPer;
           alternatePersonAccount.FirstName = 'TestAltPerson' ;
           alternatePersonAccount.LastName = 'Test' ;
           
           //inesrt Corporate Representative Account
           Account alternateIndividualMember = new Account();
           alternateIndividualMember.RecordTypeId = CorporateRepesentRecordTyprId;
           alternateIndividualMember.FirstName = 'Test ParentCorporate' ;
           alternateIndividualMember.LastName = 'Test' ;
           alternateIndividualMember.Person_Account__c = alternatePersonAccount.id;
           alternateIndividualMember.Association__c = ass.id;
           alternateIndividualMember.Member_Type__c = member.id;
           
           //insert billingPersonAccount
           Account billingPersonAccount = new Account();
           billingPersonAccount.RecordTypeId = AccRecordTypeIdPer;
           billingPersonAccount.FirstName = 'TesttPerson' ;
           billingPersonAccount.LastName = 'Test' ;
            
           //insert billing Corporate Representative 
           Account billingIndividualMember = new Account();
           billingIndividualMember.RecordTypeId = CorporateRepesentRecordTyprId;
           billingIndividualMember.FirstName = 'TestParentBill' ;
           billingIndividualMember.LastName = 'Test' ;
           billingIndividualMember.Person_Account__c = billingPersonAccount.id;
           billingIndividualMember.Association__c = ass.id;
           billingIndividualMember.Member_Type__c = member.id;
           
           //insert legalPersonAccount
           Account legalPersonAccount = new Account();
           legalPersonAccount.RecordTypeId = AccRecordTypeIdPer;
           legalPersonAccount.FirstName = 'TestParentLegal' ;
           legalPersonAccount.LastName = 'Test' ;
           
           //insert legal Corporate Representative Account
           Account legalIndividualMember = new Account();
           legalIndividualMember.RecordTypeId = CorporateRepesentRecordTyprId;
           legalIndividualMember.FirstName = 'Test ParentLegalCorp' ;
           legalIndividualMember.LastName = 'Test' ;
           legalIndividualMember.Person_Account__c = legalPersonAccount.id;
           legalIndividualMember.Association__c = ass.id;
           legalIndividualMember.Member_Type__c = member.id;
           
           String memberRecordtypeName = 'testt';
           String selectVal ='All';
           String memberVal = 'null';
           boolean createNewBillContact = true;
           boolean createNewLegalContact = true;
           String billselectVal ='Primary Contact';
           String legalselectval = 'Primary Contact';
           Id assMemberType = member.id;
           Id associationId =ass.id;
            
           AddMemberAssociationCTRL.getAssMemberType(ass.id,memberRecordtypeName);
           AddMemberAssociationCTRL.getAssCorporateMember(ass.id,selectVal,null);
           AddMemberAssociationCTRL.insertNewCorporateAccount(acc,accMem,accPer,accInd,alternatePersonAccount,alternateIndividualMember,billingPersonAccount,
                                                                billingIndividualMember,legalPersonAccount,legalIndividualMember,assMemberType,associationId,null,billselectVal,legalselectval,
                                                                createNewBillContact,createNewLegalContact);
          
      }
      
      public static testmethod void existingCorporateAccount(){
           Association__c ass = new Association__c();
           ass.Name = 'test';
           insert ass;
            
           Member_Type__c member =new Member_Type__c();
           member.Association__c =ass.id;
           member.Name ='Test';
           insert member;
            
           Id AccRecordTypeIdPer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
           Account accPer = new Account();
           accPer.RecordTypeId = AccRecordTypeIdPer;
           accPer.FirstName = 'RajaPerson' ;
           accPer.LastName = 'Test' ;
                 
         //insert Individual Member Account  
           Id AccRecordTypeIdInd = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
           Account accInd = new Account();
           accInd.RecordTypeId = AccRecordTypeIdInd;
           accInd.FirstName = 'Test ParentIndividual' ;
           accInd.LastName = 'Test' ;
           accInd.Person_Account__c = accPer.id;
           accInd.Association__c = ass.id;
           accInd.Member_Type__c = member.id;
           
           Id corptAccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Account').getRecordTypeId(); 
           Account corporateAccount = new Account();
           corporateAccount.RecordTypeId=corptAccRecordTypeId;
           corporateAccount.Association__c=ass.id;
           corporateAccount.Name = 'test';
           insert corporateAccount;
            
            //insert alternatePersonAccount 
           Account alternatePersonAccount = new Account();
           alternatePersonAccount.RecordTypeId = AccRecordTypeIdPer;
           alternatePersonAccount.FirstName = 'Test ParentAlt' ;
           alternatePersonAccount.LastName = 'Test' ;
           
           //inesrt alternateIndividualMember Account
           Account alternateIndividualMember = new Account();
           alternateIndividualMember.RecordTypeId = AccRecordTypeIdInd;
           alternateIndividualMember.FirstName = 'Test ParentAliInv' ;
           alternateIndividualMember.LastName = 'Test' ;
           alternateIndividualMember.Person_Account__c = alternatePersonAccount.id;
           alternateIndividualMember.Association__c = ass.id;
           alternateIndividualMember.Member_Type__c = member.id;
           
           //insert billingPersonAccount
           Account billingPersonAccount = new Account();
           billingPersonAccount.RecordTypeId = AccRecordTypeIdPer;
           billingPersonAccount.FirstName = 'Test ParentBillPerson' ;
           billingPersonAccount.LastName = 'Test' ;
            
           //insert billingIndividualMember 
           Account billingIndividualMember = new Account();
           billingIndividualMember.RecordTypeId = AccRecordTypeIdInd;
           billingIndividualMember.FirstName = 'Test ParentBill' ;
           billingIndividualMember.LastName = 'Test' ;
           billingIndividualMember.Person_Account__c = billingPersonAccount.id;
           billingIndividualMember.Association__c = ass.id;
           billingIndividualMember.Member_Type__c = member.id;
           
           //insert legalPersonAccount
           Account legalPersonAccount = new Account();
           legalPersonAccount.RecordTypeId = AccRecordTypeIdPer;
           legalPersonAccount.FirstName = 'TestTesParent' ;
           legalPersonAccount.LastName = 'Test' ;
           
           //insert legalIndividualMember Account
           Account legalIndividualMember = new Account();
           legalIndividualMember.RecordTypeId = AccRecordTypeIdInd;
           legalIndividualMember.FirstName = 'Test ParentLegInv' ;
           legalIndividualMember.LastName = 'Test' ;
           legalIndividualMember.Person_Account__c = legalPersonAccount.id;
           legalIndividualMember.Association__c = ass.id;
           legalIndividualMember.Member_Type__c = member.id;
           
           String memberRecordtypeName = 'test';
           String selectVal ='Active';
           boolean createNewBillContact = true;
           boolean createNewLegalContact = true;
           AddMemberAssociationCTRL.getAssMemberType(ass.id,memberRecordtypeName);
           AddMemberAssociationCTRL.getAssCorporateMember(ass.id,selectVal,null);
            
           Id AccRecordTypeIdPerTest = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
           Account accPerTest = new Account();
           accPerTest.RecordTypeId = AccRecordTypeIdPerTest;
           accPerTest.FirstName = 'RajaPersonAcc' ;
           accPerTest.LastName = 'Test' ;
           insert accPerTest;
           
           Id legalcontpersonAccId = accPerTest.id;
           AddMemberAssociationCTRL.existingCorporateAccount(accPer,accInd,corporateAccount.id,alternatePersonAccount,alternateIndividualMember,
                                                                billingPersonAccount,billingIndividualMember,legalPersonAccount,legalIndividualMember,member.id,
                                                                 ass.id,accPerTest.id,legalcontpersonAccId,'Other','Other',
                                                                  createNewBillContact,createNewLegalContact);
      
      }
      
      public static testmethod void insertNewIndividualMember(){
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
            
            Member_Type__c member =new Member_Type__c();
            member.Association__c =ass.id;
            member.Name ='Test';
            insert member;
            
            Id AccRecordTypeIdPer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account accPer = new Account();
            accPer.RecordTypeId = AccRecordTypeIdPer;
            accPer.FirstName = 'Raja' ;
            accPer.LastName = 'Test' ;
           
           //insert Individual Member Account  
            Id AccRecordTypeIdInd = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
            Account accInd = new Account();
            accInd.RecordTypeId = AccRecordTypeIdInd;
            accInd.FirstName = 'Test Parent' ;
            accInd.LastName = 'Test' ;
            accInd.Person_Account__c = accPer.id;
            accInd.Association__c = ass.id;
            accInd.Member_Type__c = member.id;
               
            String memberRecordtypeName = 'test';
            String selectVal ='Active';
          
            AddMemberAssociationCTRL.insertNewIndividualMember(accPer,accInd,member.id,ass.id);
      }
      public static testmethod void insertNewStaffMember(){
           Association__c ass = new Association__c();
           ass.Name = 'test';
           insert ass;
            
           Member_Type__c member =new Member_Type__c();
           member.Association__c =ass.id;
           member.Name ='Test';
           insert member;
            
           Id AccRecordTypeIdPer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
           Account accPer = new Account();
           accPer.RecordTypeId = AccRecordTypeIdPer;
           accPer.FirstName = 'Raja' ;
           accPer.LastName = 'Test' ;
           
          //insert Individual Member Account  
           Id AccRecordTypeIdInd = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
           Account accInd = new Account();
           accInd.RecordTypeId = AccRecordTypeIdInd;
           accInd.FirstName = 'Test Parent' ;
           accInd.LastName = 'Test' ;
           accInd.Person_Account__c = accPer.id;
           accInd.Association__c = ass.id;
           accInd.Member_Type__c = member.id;
           
           String memberRecordtypeName = 'test';
           String selectVal ='Active';
          
           AddMemberAssociationCTRL.insertNewStaffMember(accPer,accInd,member.id,ass.id);
      }
      public static testmethod void fetchDomainCorporateAccount(){
           Association__c ass = new Association__c();
           ass.Name = 'test';
           insert ass;
            
           Member_Type__c member =new Member_Type__c();
           member.Association__c =ass.id;
           member.Name ='Test';
           insert member;
            
           Id AccRecordTypeIdPer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
           Account accPer = new Account();
           accPer.RecordTypeId = AccRecordTypeIdPer;
           accPer.FirstName = 'Raja' ;
           accPer.LastName = 'Test' ;
           insert accPer;
            
           //insert Corporate Account       
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Account').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.Name = 'ASC' ;
           acc.Person_Account__c=accPer.id;
           acc.Association__c = ass.id;
           insert acc;
           
          // insert domain
           Domain__c objdomain = new Domain__c();
           objdomain.Account__c = acc.id;
           objdomain.Domain_URL__c ='https:domain.com';
           insert objdomain;
           
           //insert contact
           Contact objCon =new Contact();
           objCon.LastName ='Test';
           objCon.AccountId =acc.id;
           insert objCon;
            
           String domain ='https:domain.com';
            String selectVal ='All';
           AddMemberAssociationCTRL.fetchDomainCorporateAccount(domain);
           AddMemberAssociationCTRL.fetchCorporatePersonAccount(acc.id);
           AddMemberAssociationCTRL.getAssCorporateMember(ass.id,selectVal,'test');
      
      }
     public static testmethod void insertNewCorporateRepresentative(){
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
            
            Member_Type__c member =new Member_Type__c();
            member.Association__c =ass.id;
            member.Name ='Test';
            insert member;
            
           Id corptAccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Account').getRecordTypeId(); 
           Account corporateAccount = new Account();
           corporateAccount.RecordTypeId=corptAccRecordTypeId;
           corporateAccount.Association__c=ass.id;
           corporateAccount.Name = 'test';
           insert corporateAccount;
            
           Id corptMemRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId(); 
           Account corporateMem = new Account();
           corporateMem.RecordTypeId=corptMemRecordTypeId;
           corporateMem.Association__c=ass.id;
           corporateMem.Name = 'test';
           corporateMem.ParentId=corporateAccount.id;
           insert corporateMem;
            
           Id AccRecordTypeIdPer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
           Account accPer = new Account();
           accPer.RecordTypeId = AccRecordTypeIdPer;
           accPer.FirstName = 'Raja' ;
           accPer.LastName = 'Test' ;
           
           Id AccRecordTypeIdRep = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId(); 
           Account accRep = new Account();
           accRep.RecordTypeId = AccRecordTypeIdRep;
           accRep.LastName = 'Test' ;
           accRep.Person_Account__c = accPer.id;
           accRep.Association__c = ass.id;
           accRep.Member_Type__c = member.id;
           accRep.Association_Account__c=corporateMem.id;
           
           String memberRecordtypeName = 'test';
           String selectVal ='Active';
           Id assCorporateMemberId =member.id;
           AddMemberAssociationCTRL.insertNewCorporateRepresentative(accPer,accRep,corporateMem.id,ass.id);
           AddMemberAssociationCTRL.getBaseUrl();
           AddMemberAssociationCTRL.getUIThemeDescription();
           AddMemberAssociationCTRL.getAssCorporateMember(ass.id,selectVal,'test');
      }
 }