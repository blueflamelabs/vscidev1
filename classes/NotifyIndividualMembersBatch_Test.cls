@isTest
public class NotifyIndividualMembersBatch_Test {

    /**
    * Method Name : expirationEmailAlertPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios of expirationEmailAlert functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    @isTest
    public static void expirationEmailAlertPositiveTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 5;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 4;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c= 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'monali.nagpure@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert objAssociationToInsert;
        // Create related Individual Member and Corporate Representative Account
         if(objAssociationToInsert.Id != null) {
            // Insert Individual Member
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        Individual_Members_Email_Alerts_Setting__c settingToBeUpdated = new Individual_Members_Email_Alerts_Setting__c();
        settingToBeUpdated = Individual_Members_Email_Alerts_Setting__c.getOrgDefaults();
        settingToBeUpdated.Account_Record_Type_Name__c = 'IndividualAccount,Corporate_Representative';
        update settingToBeUpdated;
        // Call batch class
        Test.startTest();
        Integer emailbefore = Limits.getEmailInvocations();
        //System.debug('emailbefore======='+emailbefore);
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Integer invocations = Limits.getEmailInvocations();
        //System.debug('invocations======='+invocations);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : expirationEmailAlertNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios of expirationEmailAlert functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    @isTest
    public static void expirationEmailAlertNegativeTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 5;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 4;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c= 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'amruta.vaidya@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        Integer emailbefore = Limits.getEmailInvocations();
        //System.debug('emailbefore======='+emailbefore);
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : firstRenewalEmailAlertPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios for firstRenewalEmailAlert fucntionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    @isTest
    public static void firstRenewalEmailAlertPositiveTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 4;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c= 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'monali.nagpure@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : firstRenewalEmailAlertNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios for firstRenewalEmailAlert fucntionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    @isTest
    public static void firstRenewalEmailAlertNegativeTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 4;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 3;
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : secondRenewalEmailAlertPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios for secondRenewalEmailAlert fucntionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    @isTest
    public static void secondRenewalEmailAlertPositiveTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 10;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        //objAssociationToInsert.Email_Sender_Address__c = 'monali.nagpure@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : secondRenewalEmailAlertNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios for secondRenewalEmailAlert fucntionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    @isTest
    public static void secondRenewalEmailAlertNegativeTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 10;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : thirdRenewalEmailAlertPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios for thirdRenewalEmailAlert functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 230819
    **/
    @isTest
    public static void thirdRenewalEmailAlertPositiveTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 20;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        //objAssociationToInsert.Email_Sender_Address__c = 'monali.nagpure@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : thirdRenewalEmailAlertNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios for thirdRenewalEmailAlert functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 230819
    **/
    @isTest
    public static void thirdRenewalEmailAlertNegativeTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 20;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : secondExpirationPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios for second expiration functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 230819
    **/
    @isTest
    public static void secondExpirationPositiveTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 20;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c= 5;
        objAssociationToInsert.Second_Expiration_Alert_Send_Days__c = 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'monali.nagpure@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : secondExpirationNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios for second expiration functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 230819
    **/
    @isTest
    public static void secondExpirationNegativeTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 20;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c= 5;
        objAssociationToInsert.Second_Expiration_Alert_Send_Days__c = 3;
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : finalExpirationPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios for final termination notice functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 230819
    **/
    @isTest
    public static void finalExpirationPositiveTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 20;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c= 5;
        objAssociationToInsert.Second_Expiration_Alert_Send_Days__c = 4;
        objAssociationToInsert.Final_Termination_Notice_Send_Days__c = 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'monali.nagpure@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : finalExpirationNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios for final termination notice functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 230819
    **/
    @isTest
    public static void finalExpirationNegativeTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 20;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c= 5;
        objAssociationToInsert.Second_Expiration_Alert_Send_Days__c = 4;
        objAssociationToInsert.Final_Termination_Notice_Send_Days__c = 3;
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : singleIndividualMemberTest
    * Parameters  : 
    * Description : Used to test the functionality when Id of individual member is passed as parameter
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 230819
    **/
    @isTest
    public static void singleIndividualMemberTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 20;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c= 5;
        objAssociationToInsert.Second_Expiration_Alert_Send_Days__c = 4;
        objAssociationToInsert.Final_Termination_Notice_Send_Days__c = 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'amruta.vaidya@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
        Account objAccToInsert = new Account();
         if(objAssociationToInsert.Id != null) {
            objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch(objAccToInsert.Id, objAccToInsert.Association__c);
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : expirationEmailAlertBulkTest
    * Parameters  : 
    * Description : Used to test the scenarios of expirationEmailAlert functionality for bulk records
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void expirationEmailAlertBulkTest() {
        // Create Association records
        List<Association__c> listAssociations = new List<Association__c>();
        List<Account> listAccount = new List<Account>();
        listAssociations = TestDataFactory.createBulkAssociationRecords(5);
        insert listAssociations;
        // Create related Individual Member Account
        if(!listAssociations.isEmpty()) {
         	listAccount = TestDataFactory.createBulkAccountRecords(listAssociations);
            insert listAccount;
        } // End of if
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : fallBackFunctionalityTest
    * Parameters  : 
    * Description : Used to test the fall back email functionality
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    @isTest
    public static void fallBackFunctionalityTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 10;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 6;
        //objAssociationToInsert.Email_Sender_Address__c = 'amruta.vaidya@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : firstRenewalFirstExpirationTest
    * Parameters  : 
    * Description : Used to test the multiple email functionality for same Individual Member
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    @isTest
    public static void firstRenewalFirstExpirationTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 4;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'amruta.vaidya@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            objAccToInsert.Expiration_Manual_Extension__c = System.today() - 3;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : secondRenewalFirstExpirationTest
    * Parameters  : 
    * Description : Used to test the multiple email functionality for same Individual Member
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    @isTest
    public static void secondRenewalFirstExpirationTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 10;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'amruta.vaidya@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            objAccToInsert.Expiration_Manual_Extension__c = System.today() - 3;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : thirdRenewalFirstExpirationTest
    * Parameters  : 
    * Description : Used to test the multiple email functionality for same Individual Member
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    @isTest
    public static void thirdRenewalFirstExpirationTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 10;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'amruta.vaidya@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = TestDataFactory.getchildAcct();
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            objAccToInsert.Expiration_Manual_Extension__c = System.today() - 3;
            insert objAccToInsert;
            // Insert Corporate Representative
            Account objCRAccToInsert = TestDataFactory.getCorporateRepresentativeAccount();
            objCRAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objCRAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
    /**
    * Method Name : customSettingNullTest
    * Parameters  : 
    * Description : Used to test the multiple email functionality for same Individual Member
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 250719
    **/
    @isTest
    public static void customSettingNullTest() {
        // Create Association records
        Association__c objAssociationToInsert = TestDataFactory.createAssociationRecords();
        // Populate expiration date and Renewal date values
        objAssociationToInsert.First_Renewal_Alert_Send_Days__c = 14;
        objAssociationToInsert.Second_Renewal_Alert_Send_Days__c = 10;
        objAssociationToInsert.Third_Renewal_Alert_Send_Days__c = 7;
        objAssociationToInsert.First_Expiration_Alert_Send_Days__c = 3;
        //objAssociationToInsert.Email_Sender_Address__c = 'amruta.vaidya@theblueflamelabs.com';
        objAssociationToInsert.Email_Sender_Address__c = 'test@gmail.com';
        insert objAssociationToInsert;
        // Create related Individual Member Account
         if(objAssociationToInsert.Id != null) {
            Account objAccToInsert = new Account();
            objAccToInsert.firstName = 'Test Individual' ;
            objAccToInsert.LastName = 'Member';
            objAccToInsert.Expiration_Manual_Extension__c = System.today() - 3;
            objAccToInsert.Next_Renewal_Date__c = System.today() + 7;
            objAccToInsert.PersonEmail = 'test@gmail.com';
            objAccToInsert.Member_Category__pc = 'Primary Contact';
            objAccToInsert.Association__c = objAssociationToInsert.Id;
            insert objAccToInsert;
        }
        // Call batch class
        Test.startTest();
        NotifyIndividualMembersBatch objBatch = new NotifyIndividualMembersBatch();
        DataBase.executeBatch(objBatch);
        Test.stopTest();
        List<Account> listAcc = [SELECT Id,
                                        Name
                                   FROM Account
                                  WHERE Member_Category__pc = 'Primary Contact'];
        System.assert(listAcc.size() > 0, 'Person Account not created');
    }
}