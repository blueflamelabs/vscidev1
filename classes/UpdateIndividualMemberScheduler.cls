/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         150719      VennScience_BFL_Monali       This Scheduler is built to execute the batch apex
**********************************************************************************************************************************************************/
global class UpdateIndividualMemberScheduler implements Schedulable {
    
    /**
    * Method Name : scheduleBatchForEveryMidnight
    * Parameters  : 
    * Description : Used to call the scheduler and schedule the batch class using cron expression
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 240719
    **/
    global void scheduleBatchForEveryMidnight() {
        String SCHEDULE_JOB_NAME = 'UpdateIndividualMember Batch';
        Set<String> jobState = new Set<String>{'COMPLETE', 'ERROR', 'DELETED'};
        Boolean isJobRunning = false;
        for(CronTrigger ct :[select Id, State from CronTrigger where CronJobDetail.Name = :SCHEDULE_JOB_NAME]){
            if(jobState.contains(ct.State) || Test.isRunningTest()) {
                System.abortJob(ct.Id);                        
            }
            else {
                isJobRunning = true;                         
            }
        }
       if(!isJobRunning){
           String cronExp = '0 0 0 * * ?';
           String jobID = system.schedule('UpdateIndividualMember Batch', cronExp, new UpdateIndividualMemberScheduler());
       }
    }
    global void execute(SchedulableContext ctx) {
        updateIndividualMemberBatch objBatch = new updateIndividualMemberBatch(); 
        database.executebatch(objBatch);
    }
}