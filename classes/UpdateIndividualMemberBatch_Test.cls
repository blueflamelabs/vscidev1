/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         230819      VennScience_BFL_Monali       This class is built to test the UpdateIndividualMemberBatch functionality.
**********************************************************************************************************************************************************/ 
@isTest
public class UpdateIndividualMemberBatch_Test {
    
    /**
    * Method Name : positiveTesForUpdateTerminationDate
    * Parameters  : 
    * Description : Used to test the positive scenarios to set the Termination Date functionality
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 230819
    **/
    @isTest
    public static void positiveTesForUpdateTerminationDate() {
        
        // Get Custom Setting record
        Termination_Date_Update_Setting__c settings = new Termination_Date_Update_Setting__c();
        settings = TestDataFactory.getTerminationCustomSettingRecord();

        // Get Corporate Record Type Id from custom setting
        String corporateRecordType = settings.Corporate_Member_RecordType_Name__c;
        //Id corporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Corporate Member').getRecordTypeId();
        Id corporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
            											  .get(corporateRecordType)
                                                          .getRecordTypeId();
        // Get Association record
        Association__c associationRec = new Association__c();
        associationRec = TestDataFactory.createAssociationRecords();
        associationRec.Final_Termination_Notice_Send_Days__c = 10;
        insert associationRec;
        
        // Insert Corporate Account record
        Account parentAcountId = new Account();
        parentAcountId.RecordTypeId = corporateRecTypeId;
        parentAcountId.Name = 'Test Parent' ;
        parentAcountId.Expiration_Manual_Extension__c = System.today() - 10;
        parentAcountId.Company_Association_Membership__c = associationRec.Id;
        insert parentAcountId;
       
        Account childAccount = new Account();
        childAccount = TestDataFactory.getchildAcct();
        childAccount.Association_Account__c = parentAcountId.Id;
        childAccount.Expiration_Manual_Extension__c = System.today() + 3;
        insert childAccount;
        
        Test.startTest();
        
        updateIndividualMemberBatch objRec = new updateIndividualMemberBatch();
        DataBase.executeBatch(objRec);
        Test.stopTest();
        Account updatedChildAccRecord = new Account();
        Account updatedParentAccRecord = new Account();
        updatedParentAccRecord = [SELECT Id,
                                         Termination_Date__c
                                    FROM Account
                                   WHERE Id = :parentAcountId.Id];
        updatedChildAccRecord = [SELECT Id,
                                        Termination_Date__c
                                   FROM Account
                                  WHERE Id = :childAccount.Id];
        // Assert: Check if Termination Date has been updated for Individual Member
        System.assert(updatedChildAccRecord.Termination_Date__c != null);
        // Assert: Check if Termination Date has been updated for Corporate Member
        System.assert(updatedParentAccRecord.Termination_Date__c != null);
        
    }
    /**
    * Method Name : testForUpdateTerminationDate
    * Parameters  : 
    * Description : Used to test the positive scenarios to set the Termination Date functionality
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 230819
    **/
    @isTest
    public static void testForUpdateTerminationDate() {
        
        // Get Custom Setting record
        Termination_Date_Update_Setting__c settings = new Termination_Date_Update_Setting__c();
        settings = TestDataFactory.getTerminationCustomSettingRecord();
		// Get Corporate Record Type Id from custom setting
        String corporateRecordType = settings.Corporate_Member_RecordType_Name__c;
        Id corporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
            											  .get(corporateRecordType)
                                                          .getRecordTypeId();
        
        // Get Association record
        Association__c associationRec = new Association__c();
        associationRec = TestDataFactory.createAssociationRecords();
        insert associationRec;
        
        // Create Corporate Account record
        Account parentAcountId = new Account();
        parentAcountId.RecordTypeId = corporateRecTypeId;
        parentAcountId.Name = 'Test Parent' ;
        parentAcountId.Company_Association_Membership__c = associationRec.Id;
        parentAcountId.Expiration_Manual_Extension__c = System.today();
        insert parentAcountId;
        
        // Create Individual Member Account record
        Account childAccount = new Account();
        childAccount = TestDataFactory.getchildAcct();
        childAccount.Association_Account__c = parentAcountId.Id;
        childAccount.Expiration_Manual_Extension__c = System.today() + 3;
        childAccount.firstName = 'Test Individual' ;
        childAccount.LastName = 'Member';
        childAccount.Expiration_Manual_Extension__c = System.today() + 3;
        childAccount.Next_Renewal_Date__c = System.today() + 7;
        childAccount.Member_Category__pc = 'Primary Contact';
        insert childAccount;
        
        Test.startTest();
        
        updateIndividualMemberBatch objRec = new updateIndividualMemberBatch();
        DataBase.executeBatch(objRec);
        Test.stopTest();
        Account updatedChildAccRecord = new Account();
        Account updatedParentAccRecord = new Account();
        updatedParentAccRecord = [SELECT Id,
                                         Termination_Date__c
                                    FROM Account
                                   WHERE Id = :parentAcountId.Id];
        updatedChildAccRecord = [SELECT Id,
                                        Termination_Date__c
                                   FROM Account
                                  WHERE Id = :childAccount.Id];
        // Assert: Check if Termination Date has been updated for Individual Member
        System.assert(updatedChildAccRecord.Termination_Date__c != null);
        // Assert: Check if Termination Date has been updated for Corporate Member
        System.assert(updatedParentAccRecord.Termination_Date__c != null);
        
    }
    /**
    * Method Name : indMemFinalTerminationNotNullTest
    * Parameters  : 
    * Description : Used to test the scenario when Final Termination Date for Parent Association is not null
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void indMemFinalTerminationNotNullTest() {
        
        // Get Custom Setting record
        Termination_Date_Update_Setting__c settings = new Termination_Date_Update_Setting__c();
        settings = TestDataFactory.getTerminationCustomSettingRecord();
        // Get Corporate Record Type Id from custom setting
        String corporateRecordType = settings.Corporate_Member_RecordType_Name__c;
        //Id corporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Corporate Member').getRecordTypeId();
        Id corporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
            											  .get(corporateRecordType)
                                                          .getRecordTypeId();
        
        // Get Association record
        Association__c associationRec = new Association__c();
        associationRec = TestDataFactory.createAssociationRecords();
        associationRec.Final_Termination_Notice_Send_Days__c = 10;
        insert associationRec;
        
        Account parentAcountId = new Account();
        parentAcountId.RecordTypeId = corporateRecTypeId;
        parentAcountId.Name = 'Test Parent' ;
        parentAcountId.Company_Association_Membership__c = associationRec.Id;
        parentAcountId.Expiration_Manual_Extension__c = System.today();
        insert parentAcountId;
        
        // Create Individual Member Account record
        Account childAccount = new Account();
        childAccount = TestDataFactory.getchildAcct();
        childAccount.Association__c = associationRec.Id;
        childAccount.Expiration_Manual_Extension__c = System.today() - 10;
        childAccount.Next_Renewal_Date__c = System.today() + 7;
        childAccount.Member_Category__pc = 'Primary Contact';
        insert childAccount;
        
        Test.startTest();
        updateIndividualMemberBatch objRec = new updateIndividualMemberBatch();
        DataBase.executeBatch(objRec);
        Test.stopTest();
        Account updatedAccountRecord = new Account();
        updatedAccountRecord = [SELECT Id,
                                       Termination_Date__c
                                  FROM Account
                                 WHERE Id = :childAccount.Id];
         System.assert(updatedAccountRecord.Termination_Date__c != null);
        
    }
    /**
    * Method Name : indMemFinalTerminationNullTest
    * Parameters  : 
    * Description : Used to test the scenario when Final Termination Date for Parent Association is null
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void indMemFinalTerminationNullTest() {
        
        // Get Custom Setting record
        Termination_Date_Update_Setting__c settings = new Termination_Date_Update_Setting__c();
        settings = TestDataFactory.getTerminationCustomSettingRecord();
		// Get Corporate Record Type Id from custom setting
        String corporateRecordType = settings.Corporate_Member_RecordType_Name__c;
        //Id corporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Corporate Member').getRecordTypeId();
        Id corporateRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
            											  .get(corporateRecordType)
                                                          .getRecordTypeId();
        
        // Get Association record
        Association__c associationRec = new Association__c();
        associationRec = TestDataFactory.createAssociationRecords();
        associationRec.Final_Termination_Notice_Send_Days__c = null;
        insert associationRec;
        
        // Create Corporate Account record
        Account parentAcountId = new Account();
        parentAcountId.RecordTypeId = corporateRecTypeId;
        parentAcountId.Name = 'Test Parent' ;
        parentAcountId.Company_Association_Membership__c = associationRec.Id;
        parentAcountId.Expiration_Manual_Extension__c = System.today();
        insert parentAcountId;
        
        // Create Individual Member Account record
        Account childAccount = new Account();
        childAccount = TestDataFactory.getchildAcct();
        childAccount.Association__c = associationRec.Id;
        childAccount.Expiration_Manual_Extension__c = System.today();
        childAccount.Next_Renewal_Date__c = System.today();
        childAccount.Member_Category__pc = 'Primary Contact';
        insert childAccount;
        
        Test.startTest();
        updateIndividualMemberBatch objRec = new updateIndividualMemberBatch();
        DataBase.executeBatch(objRec);
        Test.stopTest();
        Account updatedAccountRecord = new Account();
        updatedAccountRecord = [SELECT Id,
                                       Termination_Date__c
                                  FROM Account
                                 WHERE Id = :childAccount.Id];
         System.assert(updatedAccountRecord.Termination_Date__c != null);
        
    }
    /**
    * Method Name : customSettingNullTest
    * Parameters  : 
    * Description : Used to test the scenario when Custom Setting is null
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
	@isTest
    public static void customSettingNullTest() {

        // Create Association record
        Association__c associationRec = new Association__c();
        associationRec.Name = 'Test ASS';
        associationRec.Association_Full_Name__c = 'Test Association';
        associationRec.Final_Termination_Notice_Send_Days__c = null;
        insert associationRec;
        
        Account parentAcountId = new Account();
        //parentAcountId.RecordTypeId = corporateRecTypeId;
        parentAcountId.Name = 'Test Parent' ;
        parentAcountId.Company_Association_Membership__c = associationRec.Id;
        parentAcountId.Expiration_Manual_Extension__c = System.today();
        insert parentAcountId;
        
        // Create Individual Member Account record
        Account childAccount = new Account();
        childAccount = TestDataFactory.getchildAcct();
        childAccount.Association__c = associationRec.Id;
        childAccount.Expiration_Manual_Extension__c = System.today();
        childAccount.Next_Renewal_Date__c = System.today();
        childAccount.Member_Category__pc = 'Primary Contact';
        insert childAccount;
        
        Test.startTest();
        updateIndividualMemberBatch objRec = new updateIndividualMemberBatch();
        DataBase.executeBatch(objRec);
        Test.stopTest();
        Account updatedAccountRecord = new Account();
        updatedAccountRecord = [SELECT Id,
                                       Termination_Date__c
                                  FROM Account
                                 WHERE Id = :childAccount.Id];
         System.assert(updatedAccountRecord.Termination_Date__c == null);
    }
    /**
    * Method Name : customSettingNullTest
    * Parameters  : 
    * Description : Used to test the scenario for Bulk Association records
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    /*
	@isTest
    public static void terminationDateUpdationBulkTest() {
        // Get Custom Setting record
        Termination_Date_Update_Setting__c settings = new Termination_Date_Update_Setting__c();
        settings = TestDataFactory.getTerminationCustomSettingRecord();
        // Create Association records
        List<Association__c> listAssociations = new List<Association__c>();
        List<Association__c> listUpdatedAssociations = new List<Association__c>();
        listAssociations = TestDataFactory.createBulkAssociationRecords(5);
        for(Association__c associationRecord : listAssociations) {
            associationRecord.Final_Termination_Notice_Send_Days__c = null;
            listUpdatedAssociations.add(associationRecord);
        }
        insert listUpdatedAssociations;
        
        // Create Individual Member Account record
        List<Account> listChildAccount = new List<Account>();
        List<Account> listUpdatedChildAccount = new List<Account>();
        if(!listAssociations.isEmpty()) {
            listChildAccount = TestDataFactory.createBulkAccountRecords(listUpdatedAssociations);
            for(Account accRecord : listChildAccount) {
        		accRecord.Expiration_Manual_Extension__c = System.today();
        		accRecord.Next_Renewal_Date__c = System.today();
                listUpdatedChildAccount.add(accRecord);
            }
            insert listUpdatedChildAccount;
    	} // End of if
        Test.startTest();
        updateIndividualMemberBatch objRec = new updateIndividualMemberBatch();
        DataBase.executeBatch(objRec);
        Test.stopTest();
        List<Account> listUpdatedAccounts = new List<Account>();
        listUpdatedAccounts = [SELECT Id,
                                      Termination_Date__c
                                 FROM Account
                                WHERE Member_Category__pc = 'Primary Contact'];
        for(Account accRecord : listUpdatedAccounts) {
            System.assert(accRecord.Termination_Date__c != null);
        }
    }
	*/
}