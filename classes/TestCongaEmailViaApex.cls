public class TestCongaEmailViaApex {

    public static String testCongaBackgroundMode() {

        String sessId = UserInfo.getSessionId();
        String servUrl = Url.getSalesforceBaseUrl().toExternalForm()+'/services/Soap/u/29.0/'+UserInfo.getOrganizationId();
        Set<Id> setPersonConId = new Set<Id>();
            
        /*String url2 = '/apex/APXTConga4__Conga_Composer'+
                              '?sessionId='+sessId+
                              '&serverUrl='+EncodingUtil.urlEncode(servUrl, 'UTF-8')+
                              '&id=006g0000004b6cB'+
                              '&TemplateId=a0m0m000002ZPFKAA4'+
                              '&SC0=1'+
                              '&SC1=Attachments'+
                              '&EmailToID=0030m00000QiHRNAA3'+
                              '&CETID=a0d0m000000xIUcAAM'+
                              '&FP0=1'+
                              '&DS7=112'+
                              '&BML=Test+message';*/
                                                    
        /*String url2 = 'https://composer.congamerge.com'+
                              '?sessionId='+sessId+
                              '&serverUrl='+EncodingUtil.urlEncode(servUrl, 'UTF-8')+
                              '&id=a0n0m000000FmNwAAK'+
                              '&TemplateId=a0m0m000002ZPFK'+
                              '&DefaultPDF=1'+
                              '&DS7=2'+
                              '&EmailToID=0030m00000QiHRNAA3'+
                              '&CETID=a0d0m000000xIUc'+
                              '&BML=Test+message';*/
        // With verified org-wide address                       
        /*String url2 = 'https://composer.congamerge.com/composer8/index.html'+
                      '?sessionId='+UserInfo.getSessionId()+
                      '&serverUrl='+EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm()+
                      '/services/Soap/u/29.0/'+UserInfo.getOrganizationId(), 'UTF-8')+
                      '&Id=a0n0m000000FmNw'+
                       '&QueryId=[InvoiceLineItem]a0e0m0000001RDt,[Association]a0e0m0000001RQy,[Account]a0e0m0000001VVJ'+
                       '&TemplateId=a0m0m000002ZPFK'+
                       '&EmailFromId=0D20m000000CagR'+
                       '&APIMode=12'+
                       '&CongaEmailTemplateId=a0d0m000000xIUc'+
                       '&DefaultPDF=1'+
                       '&EmailToId=0030m00000QiHRN';*/
        // With unverified org-wide address
        /*String url2 = 'https://composer.congamerge.com/composer8/index.html'+
                      '?sessionId='+UserInfo.getSessionId()+
                      '&serverUrl='+EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm()+
                      '/services/Soap/u/29.0/'+UserInfo.getOrganizationId(), 'UTF-8')+
                      '&Id=a0n0m000000FmNw'+
                       '&QueryId=[InvoiceLineItem]a0e0m0000001RDt,[Association]a0e0m0000001RQy,[Account]a0e0m0000001VVJ'+
                       '&TemplateId=a0m0m000002ZPFK'+
                       '&EmailFromId=0D20m000000CagW'+
                       '&APIMode=12'+
                       '&CongaEmailTemplateId=a0d0m000000xIUc'+
                       '&DefaultPDF=1'+
                       '&EmailToId=0030m00000QiHRN';*/
        // with same emailTo and emailAddtionalTo values
        String url2 = 'https://composer.congamerge.com/composer8/index.html'+
                      '?sessionId='+UserInfo.getSessionId()+
                      '&serverUrl='+EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm()+
                      '/services/Soap/u/29.0/'+UserInfo.getOrganizationId(), 'UTF-8')+
                      '&Id=a0n0m000000FmNw'+
                       '&QueryId=[InvoiceLineItem]a0e0m0000001RDt,[Association]a0e0m0000001RQy,[Account]a0e0m0000001VVJ'+
                       '&TemplateId=a0m0m000002ZPFK'+
                       '&EmailFromId=0D20m000000CagR'+
                       '&APIMode=12'+
                       '&CongaEmailTemplateId=a0d0m000000xIUc'+
                       '&DefaultPDF=1'+
                       '&EmailToId=0030m00000QiHRN'+
                       '&EmailAdditionalTo=0030m00000QTfvlAAD';                      
                              
        System.debug('url2======'+url2);
        setPersonConId.add('0030m00000QiHRN');
    
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url2);
        req.setMethod('GET');
        req.setTimeout(60000);
        
        // Send the request, and return a response
        HttpResponse res = http.send(req);
    
        System.debug(res);
        if(res.getStatusCode() == 200) {
            System.debug('setPersonConId======'+setPersonConId);        
        } // End of if
        return res.getStatus() + ' => ' + res.getBody();
    }
}