public class AssociationCTRL {
    
    public class wrapperClass{
        @AuraEnabled public Account objAcc{get;set;}
        @AuraEnabled public Boolean isCheck{get;set;}
        public wrapperClass(Account objAcc,Boolean isCheck){
            this.objAcc = objAcc;
            this.isCheck = isCheck;
        }
    }
    @AuraEnabled
    public static List<Account> AccountsMemberRecord(String RecId) {
        Id CorporateRepresentativeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
        List<Account> objAccList = [Select Id,LastName,Name,Join_Date__c,Billing_Contact__pc,Association_Account__c,
                                    Member_Category__pc,Legal_Contact__pc,Association_Role__c,PersonEmail,Phone
                                    from Account where Association_Account__c =:RecId and RecordTypeId =: CorporateRepresentativeId];
        System.debug('######objAccList###3 '+objAccList);
        return objAccList;
    }
    @AuraEnabled
    public static List<wrapperClass> getAccounts(String RecId,String SearchText,Boolean isSingleChar,String RecName) {
     List<wrapperClass> objWrapList = new List<wrapperClass>(); 
        if(RecName == 'Individual Member'){
            Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecName).getRecordTypeId();
            Id CorporateRepresentativeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
            List<Account> objListAccount =  null;
            if(String.isBlank(SearchText)){
                objListAccount = [SELECT Id,LastName,Next_Renewal_Date__c,Association_Role__c, Termination_Date__c, Name,Join_Date__c,Association_Account__c,Association_Account__r.Name,Phone,PersonMobilePhone,PersonEmail,Association__c,PhotoUrl FROM Account 
                                  where (RecordTypeId =: AccRecordTypeId OR RecordTypeId =: CorporateRepresentativeId) and Association__c =:RecId and 
                                  (Termination_Date__c > Today OR Termination_Date__c = NULL) ORDER BY Name];
                for(Account objAcc : objListAccount){
                    objWrapList.add(new wrapperClass(objAcc,false));
                }
            }else{
                String SearchTextName='';
                if(isSingleChar == false)
                    SearchTextName = '%'+SearchText+'%';
                else
                    SearchTextName = SearchText+'%';
                
                objListAccount = [SELECT Id,LastName,Next_Renewal_Date__c,Association_Role__c,Name,Termination_Date__c, Join_Date__c,Association_Account__c,Association_Account__r.Name,Phone,PersonMobilePhone,PersonEmail,Association__c,PhotoUrl FROM Account 
                                  where (RecordTypeId =: AccRecordTypeId OR RecordTypeId =: CorporateRepresentativeId)  and Association__c =:RecId and 
                                  LastName LIKE :SearchTextName 
                                  and (Termination_Date__c > Today OR Termination_Date__c = NULL) ORDER BY Name];
                for(Account objAcc : objListAccount){
                    objWrapList.add(new wrapperClass(objAcc,false));
                }
            }
        }   
        if(RecName == 'Corporate Member'){
            Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecName).getRecordTypeId();
            List<Account> objListAccount =  null;
            if(String.isBlank(SearchText)){
                objListAccount = [SELECT Id,LastName,Region_Name__c,Next_Renewal_Date__c,Association_Role__c,Member_Type__c,Member_Type__r.Name,Termination_Date__c, Name,Join_Date__c,Association_Account__c,Association_Account__r.Name,Phone,PersonMobilePhone,PersonEmail,Association__c,PhotoUrl FROM Account 
                                  where RecordTypeId =: AccRecordTypeId and Company_Association_Membership__c =:RecId and 
                                  (Termination_Date__c > Today OR Termination_Date__c = NULL) ORDER BY Name];
                for(Account objAcc : objListAccount){
                    objWrapList.add(new wrapperClass(objAcc,false));
                }
            }else{
                String SearchTextName='';
                if(isSingleChar == false)
                    SearchTextName = '%'+SearchText+'%';
                else
                    SearchTextName = SearchText+'%';
                
                objListAccount = [SELECT Id,LastName,Region_Name__c,Next_Renewal_Date__c,Association_Role__c,Name,Member_Type__c,Member_Type__r.Name,Termination_Date__c, Join_Date__c,Association_Account__c,Association_Account__r.Name,Phone,PersonMobilePhone,PersonEmail,Association__c,PhotoUrl FROM Account 
                                  where RecordTypeId =: AccRecordTypeId and Company_Association_Membership__c =:RecId and 
                                   Name LIKE :SearchTextName 
                                  and (Termination_Date__c > Today OR Termination_Date__c = NULL) ORDER BY Name];
                for(Account objAcc : objListAccount){
                    objWrapList.add(new wrapperClass(objAcc,false));
                }
            }
        }   
        return objWrapList;
    }
}