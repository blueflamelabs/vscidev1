/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         150719      VennScience_BFL_Monali       This class is built to Update the Related Child 
                                                             Account as well as Parent Account
**********************************************************************************************************************************************************/   
public class UpdateIndividualMemberController {
    
    List<Account> accotList = new List<Account>();
    Set<Account> parentAcctSet = new Set<Account>();
    //Set<Id> setOfIds = new Set<Id>();
    public Date dt = System.today();
    //method call from the batch to get all child account records and their related parent account.
    /*
    public void updateChildAccoundRecord(List<Account> acctList){ 
        System.debug('');
        for(Account ac : acctList) {
            
            if(ac.Association_Account__c != null ) {
                
                if(ac.Association_Account__r.Expiration_Date__c < dt) {
                    
                    ac.Association_Account__r.Termination_Date__c = dt;
                    ac.Termination_Date__c = dt;
                    accotList.add(ac);
                    parentAcctSet.add(ac.Association_Account__r);
                    System.debug('accotList=11='+accotList);
                }
            }else {
                if(ac.Expiration_Date__c < dt) {
                    ac.Termination_Date__c = dt;
                    accotList.add(ac);
                }
                System.debug('accotList==12=='+accotList);
            }
            
        }
        if(!accotList.isEmpty()) {
             update accotList;
        }
        List<Account> parentAccList = new List<Account>();
        parentAccList.addAll(parentAcctSet);
        if(!parentAccList.isEmpty()) {
            update parentAccList;
        }
        
    }
    */
    public void setTerminationDate(List<Account> listCorporateMembers) {
        
        // Variable Declarations
        List<Account> listIndividualMemberAccounts = new List<Account>();
        List<Account> listCorporateAccountsToBeUpdated = new List<Account>();
        List<Account> listChildIndividualMemToBeUpdated = new List<Account>();
        List<Account> listIndividualMemToBeUpdated = new List<Account>();
        List<Account> listMergedIndMemToBeUpdated = new List<Account>();
        Date todayDate = System.today();
        
        // 200819 - T-00511 - VennScience_BFL_Amruta - Fetch the record type Id for Individual Member
        Termination_Date_Update_Setting__c objCustomSetting = new Termination_Date_Update_Setting__c();
        objCustomSetting = getCustomSettingValues();
        String strIndMemRecordTypeName = objCustomSetting.Individual_Member_s_RecordType_Name__c;
        Id individualMemRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                                   .get('IndividualAccount')
                                   .getRecordTypeId();
      
        // Iterate oer CorporateMembersAccount list
        for(Account accRecord : listCorporateMembers) {
            // Set Corporate Member's Termination Date = Today if CorporateAssociation.FinalTerminationSendDays + Expiration Date = Today
            if(accRecord.Company_Association_Membership__c != null && accRecord.Termination_Date__c == null && 
               accRecord.Company_Association_Membership__r.Final_Termination_Notice_Send_Days__c != null && 
               accRecord.Expiration_Date__c.addDays(Integer.valueOf(accRecord.Company_Association_Membership__r.Final_Termination_Notice_Send_Days__c)) == todayDate) {
                // System.debug('Inside if');
                accRecord.Termination_Date__c = todayDate; 
                listCorporateAccountsToBeUpdated.add(accRecord);
                // Iterate over all the child Individual Members
                for(Account indMemberRecord : accRecord.Individuals__r) {
                    // Set the termination date for all child Individual Members
                    indMemberRecord.Termination_Date__c = todayDate; 
                    listChildIndividualMemToBeUpdated.add(indMemberRecord);
                } // End of child individual members for
            } // End of if 
            // Set Corporate Member's Termination Date = Today if CorporateAssociation.FinalTerminationSendDays is blank and
            // Corporate Member's Expiration Date = Today
            else if(accRecord.Company_Association_Membership__c != null && accRecord.Termination_Date__c == null && 
                   accRecord.Company_Association_Membership__r.Final_Termination_Notice_Send_Days__c == null && 
                   accRecord.Expiration_Date__c == todayDate) {
                accRecord.Termination_Date__c = todayDate;
                // Add to list
                listCorporateAccountsToBeUpdated.add(accRecord);
                // Iterate over all the child Individual Members
                for(Account indMemberRecord : accRecord.Individuals__r) {
                    // Set the termination date for all child Individual Members
                    indMemberRecord.Termination_Date__c = todayDate; 
                    // Add to list
                    listChildIndividualMemToBeUpdated.add(indMemberRecord);
                } // End of child individual members for
            } // End of else-if block
        } // End of for
        // System.debug('listCorporateAccountsToBeUpdated==========='+listCorporateAccountsToBeUpdated);
        // System.debug('listChildIndividualMemToBeUpdated==========='+listChildIndividualMemToBeUpdated);
        
        // Fetch the Individual Members which do not have Corporate Membership but has a Individual Association Membership
        listIndividualMemberAccounts = [SELECT Id,
                                               Company_Association_Membership__c,
                                               Termination_Date__c,
                                               Expiration_Date__c,
                                               Association__r.Final_Termination_Notice_Send_Days__c
                                          FROM Account
                                         WHERE RecordTypeId = :individualMemRecordTypeId
                                           AND Association_Account__c = null
                                           AND Association__c != null
                                           AND Termination_Date__c = null
                                           AND Expiration_Date__c != null];
        // Iterate over Individual Members list
        for(Account indMemRecord : listIndividualMemberAccounts) {
            // Set Individual Member's Termination Date = Today if IndividualAssociation.FinalTerminationSendDays + ExpirationDate = Today
            if(indMemRecord.Association__r.Final_Termination_Notice_Send_Days__c != null && 
               indMemRecord.Expiration_Date__c.addDays(Integer.valueOf(indMemRecord.Association__r.Final_Termination_Notice_Send_Days__c)) == todayDate) {
               indMemRecord.Termination_Date__c = todayDate; 
               // Add to list
               listIndividualMemToBeUpdated.add(indMemRecord);
            } // End of if
            else if(indMemRecord.Association__r.Final_Termination_Notice_Send_Days__c == null && 
                    indMemRecord.Expiration_Date__c == todayDate) {
                indMemRecord.Termination_Date__c = todayDate; 
                // Add to list
                listIndividualMemToBeUpdated.add(indMemRecord);    
            } // end of else-if block
        } // End of for
        // System.debug('listIndividualMemToBeUpdated========='+listIndividualMemToBeUpdated);
        
        // Merge the Individual Members list that are to be updated into a single list
        listMergedIndMemToBeUpdated.addAll(listChildIndividualMemToBeUpdated);
        listMergedIndMemToBeUpdated.addAll(listIndividualMemToBeUpdated);
        // System.debug('listMergedIndMemToBeUpdated========='+listMergedIndMemToBeUpdated);
        
        // Update the Corporate Member records
        if(!listCorporateAccountsToBeUpdated.isEmpty()) {
            Database.SaveResult[] srList = Database.update(listCorporateAccountsToBeUpdated, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    // System.debug('Successfully updated corporate account:Account ID: ' + sr.getId());
                } else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    } // End of inner for
                } // End of else-if block
            } // End of for
        } // End of if
        
        // Update the Individual Member records
        if(!listMergedIndMemToBeUpdated.isEmpty()) {
            Database.SaveResult[] srList = Database.update(listMergedIndMemToBeUpdated, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    // System.debug('Successfully updated Individual account:Account ID: ' + sr.getId());
                } else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    } // End of inner for
                } // End of else-if block
            } // End of for
        } // End of if
    }
    /**
    * Method Name : getCustomSettingValues
    * Parameters  : 
    * Description : This method is used to get the custom setting org default values
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 210819
    **/
    public static Termination_Date_Update_Setting__c getCustomSettingValues() {
        Termination_Date_Update_Setting__c objSetting = new Termination_Date_Update_Setting__c();
        // Get org default value
        objSetting = Termination_Date_Update_Setting__c.getOrgDefaults();
        return objSetting;
    }
}