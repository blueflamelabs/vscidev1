/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                  Description
*     1.0        040919         VennScience_BFL_Amruta     This is to test the ExecuteNotifyIndMembersBatchExtension apex class.
**********************************************************************************************************************************************************/
@isTest
public class ExecuteNotifyIndMemBatchExtTest {
	/**
    * Method Name : executeBatchForIndividualMemPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios of executeBatch method for Individual Member record type
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void executeBatchForIndividualMemPositiveTest() {
        // Get Association Record
        Association__c associationRecord = new Association__c();
        associationRecord = TestDataFactory.createAssociationRecords();
        // Get Account record
        Account accRecord = new Account();
        accRecord = TestDataFactory.getchildAcct();
        accRecord.Association__c = associationRecord.Id;
        insert accRecord;
        // Populate Standard Controller
        ApexPages.StandardController sc = new ApexPages.StandardController(accRecord);
        ExecuteNotifyIndMembersBatchExtension objExtension = new ExecuteNotifyIndMembersBatchExtension(sc);
        // Call Handler method
        PageReference pageReferenceResult = objExtension.executeBatch();
        // Assert: Check if page refernece is not null
        System.assertNotEquals(null, pageReferenceResult,'Page Reference is null');
    }
    /**
    * Method Name : executeBatchForIndividualMemNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios of executeBatch method for Individual Member record type
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void executeBatchForIndividualMemNegativeTest() {
        // Get Association Record
        Association__c associationRecord = new Association__c();
        associationRecord = TestDataFactory.createAssociationRecords();
        // Get Account record
        Account accRecord = new Account();
        //accRecord = TestDataFactory.getchildAcct();
        // Populate Standard Controller
        ApexPages.StandardController sc = new ApexPages.StandardController(accRecord);
        ExecuteNotifyIndMembersBatchExtension objExtension = new ExecuteNotifyIndMembersBatchExtension(sc);
        // Call Handler method
        PageReference pageReferenceResult = objExtension.executeBatch();
        // Assert: Check if page refernece is not null
        System.assertEquals(null, pageReferenceResult,'Page Reference is not null');
    }
    /**
    * Method Name : executeBatchForCorporateRepPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios of executeBatch method for Corporate Representative record type
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 190919
    **/
    @isTest
    public static void executeBatchForCorporateRepPositiveTest() {
        // Get Association Record
        Association__c associationRecord = new Association__c();
        associationRecord = TestDataFactory.createAssociationRecords();
        // Get Corporate Representative Account record
        Account accRecord = new Account();
        accRecord = TestDataFactory.getCorporateRepresentativeAccount();
        accRecord.Association__c = associationRecord.Id;
        insert accRecord;
        // Populate Standard Controller
        ApexPages.StandardController sc = new ApexPages.StandardController(accRecord);
        ExecuteNotifyIndMembersBatchExtension objExtension = new ExecuteNotifyIndMembersBatchExtension(sc);
        // Call Handler method
        PageReference pageReferenceResult = objExtension.executeBatch();
        // Assert: Check if page refernece is not null
        System.assertNotEquals(null, pageReferenceResult,'Page Reference is null');
    }
    /**
    * Method Name : executeBatchForCorporateRepNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios of executeBatch method for Corporate Representative record type
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 190919
    **/
    @isTest
    public static void executeBatchForCorporateRepNegativeTest() {
        // Get Association Record
        Association__c associationRecord = new Association__c();
        associationRecord = TestDataFactory.createAssociationRecords();
        // Get Corporate Representative Account record
        Account accRecord = new Account();
        // Populate Standard Controller
        ApexPages.StandardController sc = new ApexPages.StandardController(accRecord);
        ExecuteNotifyIndMembersBatchExtension objExtension = new ExecuteNotifyIndMembersBatchExtension(sc);
        // Call Handler method
        PageReference pageReferenceResult = objExtension.executeBatch();
        // Assert: Check if page refernece is not null
        System.assertEquals(null, pageReferenceResult,'Page Reference is not null');
    }
}