<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AECC Technical Solutions Group</label>
    <protected>false</protected>
    <values>
        <field>Record_Id__c</field>
        <value xsi:type="xsd:string">a01f400000O2bNn</value>
    </values>
    <values>
        <field>To_Email__c</field>
        <value xsi:type="xsd:string">aecc_technical_solution_group@2p8ipc89p2n2vsyzcetlcc75ishxr00ei732kra2urjp0yp0ft.f4-47jfheaq.na59.apex.salesforce.com</value>
    </values>
</CustomMetadata>
