<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Individual Member Setting</label>
    <protected>false</protected>
    <values>
        <field>Account_Member_Category__c</field>
        <value xsi:type="xsd:string">Primary Contact</value>
    </values>
    <values>
        <field>Account_Record_Type_Name__c</field>
        <value xsi:type="xsd:string">IndividualAccount</value>
    </values>
    <values>
        <field>Default_Sender_Address__c</field>
        <value xsi:type="xsd:string">vforceadmin@virtualinc.com</value>
    </values>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
